record(mbbi, "$(PREFIX):$(CHANNEL_ID)-STAT") {
    field(DESC, "The state of the channel group.")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(ASYN_PORT), 4) State")
    field(SCAN, "I/O Intr")
    field(ZRVL, "0")
    field(ZRST, "UNKNOWN")
    field(ONVL, "1")
    field(ONST, "IOCINIT")
    field(TWVL, "2")
    field(TWST, "DISABLED")
    field(THVL, "3")
    field(THST, "ENABLED")
    field(FRVL, "4")
    field(FRST, "PROCESSING")
    field(FVVL, "5")
    field(FVST, "DEGRADED")
    field(FVSV, "MAJOR")
    field(SXVL, "6")
    field(SXST, "ERROR")
    field(SXSV, "MAJOR")
    field(SVVL, "7")
    field(SVST, "RESETTING")
    field(EIVL, "8")
    field(EIST, "DEFUNCT")
    field(EISV, "MAJOR")
    field(EIVL, "9")
    field(EIST, "READY")
}

record(longin, "$(PREFIX):$(CHANNEL_ID)-READ") {
	field(DESC, "Read GCTR value.")
	field(DTYP, "asynInt32")
	field(INP,  "@asyn($(ASYN_PORT), 4) ReadGCTR")
	field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")
}

record(bo, "$(PREFIX):$(CHANNEL_ID)-CLKS") {
	field(DESC, "Set GCTR clock source.")
	field(DTYP, "asynInt32")
	field(OUT,  "@asyn($(ASYN_PORT), 4) ClockSource")
    field(ZNAM, "Internal")
    field(ONAM, "External")
    field(PINI, "YES")
    field(VAL,  "$(CLKS_VAL="0")")
}
record(bi, "$(PREFIX):$(CHANNEL_ID)-CLKS-RBV") {
	field(DESC, "GTCR clock source readback")
	field(DTYP, "asynInt32")
	field(INP,  "@asyn($(ASYN_PORT), 4) ClockSource")
	field(SCAN, "I/O Intr")
    field(ZNAM, "Internal")
    field(ONAM, "External")
}

record(bo, "$(PREFIX):$(CHANNEL_ID)-PM") {
	field(DESC, "Set GCTR processing mode.")
	field(DTYP, "asynInt32")
	field(OUT,  "@asyn($(ASYN_PORT), 4) ProcessingMode")
    field(ZNAM, "General")
    field(ONAM, "Pulse")
    field(PINI, "YES")
    field(VAL,  "$(PM_VAL="0")")
}
record(bi, "$(PREFIX):$(CHANNEL_ID)-PM-RBV") {
	field(DESC, "GCTR processing mode readback")
	field(DTYP, "asynInt32")
	field(INP,  "@asyn($(ASYN_PORT), 4) ProcessingMode")
	field(SCAN, "I/O Intr")
    field(ZNAM, "General")
    field(ONAM, "Pulse")
}

record(bo, "$(PREFIX):$(CHANNEL_ID)-GTS") {
	field(DESC, "Set GCTR gating source.")
	field(DTYP, "asynInt32")
	field(OUT,  "@asyn($(ASYN_PORT), 4) GateSource")
    field(ZNAM, "Internal")
    field(ONAM, "External")
    field(PINI, "YES")
    field(VAL,  "$(GTS_VAL="0")")
}
record(bi, "$(PREFIX):$(CHANNEL_ID)-GTS-RBV") {
	field(DESC, "GCTR gating source readback.")
	field(DTYP, "asynInt32")
	field(INP,  "@asyn($(ASYN_PORT), 4) GateSource")
	field(SCAN, "I/O Intr")
    field(ZNAM, "Internal")
    field(ONAM, "External")
}

record(bo, "$(PREFIX):$(CHANNEL_ID)-UDS") {
	field(DESC, "Set GCTR direction source.")
	field(DTYP, "asynInt32")
	field(OUT,  "@asyn($(ASYN_PORT), 4) UpDownSource")
    field(ZNAM, "Software")
    field(ONAM, "External")
    field(PINI, "YES")
    field(VAL,  "$(UDS_VAL="0")")
}
record(bi, "$(PREFIX):$(CHANNEL_ID)-UDS-RBV") {
	field(DESC, "GCTR direction source readback.")
	field(DTYP, "asynInt32")
	field(INP,  "@asyn($(ASYN_PORT), 4) UpDownSource")
	field(SCAN, "I/O Intr")
    field(ZNAM, "Software")
    field(ONAM, "External")
}

record(bo, "$(PREFIX):$(CHANNEL_ID)-UDC") {
	field(DESC, "Set GCTR direction control")
	field(DTYP, "asynInt32")
	field(OUT,  "@asyn($(ASYN_PORT), 4) UpDownControl")
    field(ZNAM, "Up")
    field(ONAM, "Down")
    field(PINI, "YES")
    field(VAL,  "$(UDC_VAL="0")")
}
record(bi, "$(PREFIX):$(CHANNEL_ID)-UDC-RBV") {
	field(DESC, "GCTR direction control readback")
	field(DTYP, "asynInt32")
	field(INP,  "@asyn($(ASYN_PORT), 4) UpDownControl")
	field(SCAN, "I/O Intr")
    field(ZNAM, "Up")
    field(ONAM, "Down")
}

record(longout, "$(PREFIX):$(CHANNEL_ID)-COUNT") {
	field(DESC, "Set GCTR counter value.")
    field(DTYP, "asynInt32")
	field(OUT,  "@asyn($(ASYN_PORT), 4) Count")
    field(PINI, "YES")
    field(VAL,  "$(COUNT_VAL="0")")
}

record(longin, "$(PREFIX):$(CHANNEL_ID)-COUNT-RBV") {
	field(DESC, "GCTR counter value readback")
    field(DTYP, "asynInt32")
	field(INP,  "@asyn($(ASYN_PORT), 4) Count")	
	field(SCAN, "I/O Intr")
}

record(waveform, "$(PREFIX):$(CHANNEL_ID)-MSGS") {
	field(DESC, "Send message to device driver.")
	field(DTYP, "asynOctetWrite")
	field(INP,  "@asyn($(ASYN_PORT), 4) Command")
	field(FTVL, "CHAR")
	field(NELM, "255")	
	field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")
}

record(waveform, "$(PREFIX):MSGR") {
	field(DESC, "Receive message from device driver.")
	field(DTYP, "asynOctetRead")
	field(INP,  "@asyn($(ASYN_PORT), 4) Command")
	field(SCAN, "I/O Intr")	
	field(FTVL, "CHAR")
	field(NELM, "255")
	field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")
}
