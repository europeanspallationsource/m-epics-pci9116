/**
 * Adlink PCI9116 EPICS module
 * Copyright (C) 2015 Cosylab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @file pci9116AIChannel.cpp
 * @brief Implementation of cPci-9116 DAQ card analog input channel in NDS.
 * @author kstrnisa
 * @date 20.8.2013
 */

#include "ndsADIOChannel.h"
#include "ndsChannelStateMachine.h"
#include "ndsChannelStates.h"
#include "ndsChannel.h"

#include "pci9116Device.h"
#include "pci9116AIChannelGroup.h"
#include "pci9116AIChannel.h"

#include <dask.h>
#include "pcidask_errors.h"

/**
 * @brief AI channel constructor.
 */
pci9116AIChannel::pci9116AIChannel() {

    registerOnRequestStateHandler(nds::CHANNEL_STATE_DISABLED, nds::CHANNEL_STATE_PROCESSING,
            boost::bind(&pci9116AIChannel::onSwitchProcessing, this, _1, _2));

    registerOnLeaveStateHandler(nds::CHANNEL_STATE_PROCESSING,
            boost::bind(&pci9116AIChannel::onLeaveProcessing, this, _1, _2));

    registerOnEnterStateHandler(nds::CHANNEL_STATE_DISABLED,
            boost::bind(&pci9116AIChannel::onEnterDisabled, this, _1, _2));

    registerOnEnterStateHandler(nds::CHANNEL_STATE_ERROR,
            boost::bind(&pci9116AIChannel::onEnterError, this, _1, _2));

    _ChannelBuffer = NULL;
}


/**
 * @brief AI Channel destructor.
 *
 * Free voltage buffer for channel.
 */
pci9116AIChannel::~pci9116AIChannel() {
    freeBuffers();
}


/**
 * @brief Enable or disable channel.
 * @param [in] pasynUser Asyn user struct.
 * @param [out] value Disable (0) or enable (non-0) the channel.
 *
 * @retval ndsSuccess Channel enabled/disabled successfully.
 * @retval ndsError Operation not allowed.
 *
 * All enabled channel will be included in the acquisition when the AI channel
 * group goes to PROCESSING state. If the input mode of the channel group is
 * differential only channels < 31 can be enabled. Enabling/disabling channels
 * is not allowd if the channel group is processing.
 */
ndsStatus pci9116AIChannel::setEnabled(asynUser* pasynUser, epicsInt32 value) {
    pci9116AIChannelGroup   *cg;
    epicsInt32              inputMode;

    NDS_TRC("%s", __func__);

    cg = dynamic_cast<pci9116AIChannelGroup *>(getChannelGroup());

    cg->lock();

    if (cg->getCurrentState() == nds::CHANNEL_STATE_PROCESSING) {
        NDS_ERR("Operation not allowed while AI channel group is processing.");
        cg->unlock();
        return ndsError;
    }

    if (value) {
        /* If input mode is differential only first half of the channels are available. */
        cg->getDifferential(pasynUser, &inputMode);
        if (inputMode == pci9116_input_mode_differential
                && getChannelNumber() >= PCI9116NDS_NUM_AI_CHANNELS / 2) {
            NDS_ERR("Invalid channel index for differential setting.");
            cg->unlock();
            return ndsError;
        }
    }

    _isEnabled = value;

    cg->unlock();

    NDS_DBG("Channel %d %s.", getChannelNumber(), value ? "enabled" : "disabled");

    doCallbacksInt32(_isEnabled, _interruptIdEnabled);
    return ndsSuccess;
}


/**
 * @brief Set number of samples to be acquired for the channel.
 * @param [in] pasynUser Asyn user struct.
 * @param [out] value Number of samples.
 *
 * @retval ndsSuccess Parameter set successfully.
 * @retval ndsError Operation not allowed.
 *
 * The number of samples indicates how much data is expected for the channel at
 * the end of acquisition. This can only be set by the AI channel group.
 */
ndsStatus pci9116AIChannel::setSamplesCount(asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    /* Only the channel group is allowed to set samples count and it calls
     * the setter with pasynUser=NULL. */
    if (pasynUser) {
        NDS_ERR("Setting number of samples must be done through the AI channel group.");
        return ndsError;
    }

    _SamplesCount = value;
    doCallbacksInt32(_SamplesCount, _interruptIdSamplesCount);
    return ndsSuccess;
}


/**
 * @brief Set channel AD range.
 * @param [in] pasynUser Asyn user struct.
 * @param [out] value Range to set.
 *
 * @retval ndsSuccess Parameter set successfully.
 * @retval ndsError Invalid parameter value or operation not allowed.
 *
 * The possible ranges are enumerated in #pci9116_range. The choices specify
 * the peak-to-peak range. Whether range is bipolar or unipolar depends on
 * the range of the AI channel group. Changing the range of an analog channel
 * is not allowed while the channel group is processing.
 */
ndsStatus pci9116AIChannel::setRange(asynUser* pasynUser, epicsInt32 value) {
    pci9116AIChannelGroup   *cg;

    NDS_TRC("%s", __func__);

    cg = dynamic_cast<pci9116AIChannelGroup *>(getChannelGroup());

    cg->lock();

    if (cg->getCurrentState() == nds::CHANNEL_STATE_PROCESSING) {
        NDS_ERR("Operation not allowed while AI channel group is processing.");
        cg->unlock();
        return ndsError;
    }

    if (value < pci9116_range_10V || value > pci9116_range_1V25) {
        NDS_ERR("invalid range.");
        cg->unlock();
        return ndsError;
    }

    _Range = value;

    cg->unlock();

    NDS_DBG("Channel %d setting range %d.", getChannelNumber(), value);

    doCallbacksInt32(_Range, _interruptIdRange);
    return ndsSuccess;
}


/**
 * @brief Read single AI channel value.
 * @param [in] pasynUser Asyn user struct.
 * @param [out] value AI channel value in volts.
 *
 * @retval ndsSuccess Value read successfully.
 * @retval ndsError Index of the channel invalid in the current input mode,
 * card configuration failed, error reading the value from hardware or the
 * operation is not permitted.
 *
 * Configures the card with the currently selected polarity, input mode,
 * grounding choice and soft trigger type. This is used for single value reads
 * from analog channels. This operation is not allowed while the channel group is processing.
 */
ndsStatus pci9116AIChannel::getValueFloat64(asynUser* pasynUser, epicsFloat64 *value) {
    pci9116AIChannelGroup   *cg;
    int                     status;
    epicsInt32              inputMode, polarity, ground;
    epicsUInt16             configControl, range;

    NDS_TRC("%s", __func__);

    cg = dynamic_cast<pci9116AIChannelGroup *>(getChannelGroup());

    cg->lock();

    if (_device->getCurrentState() != nds::DEVICE_STATE_ON) {
        NDS_ERR("Channel value can be read only when device is in ON state.");
        cg->unlock();
        return ndsError;
    }

    if (cg->getCurrentState() != nds::CHANNEL_STATE_DISABLED) {
        NDS_ERR("Channel value can be read only when AI channel group is in DISABLED state.");
        cg->unlock();
        return ndsError;
    }

    /* If input mode is differential only first half of the channels are available. */
    cg->getDifferential(pasynUser, &inputMode);
    if (inputMode == pci9116_input_mode_differential
            && getChannelNumber() >= PCI9116NDS_NUM_AI_CHANNELS / 2) {
        NDS_ERR("Invalid channel index for differential setting.");
        cg->unlock();
        return ndsError;
    }

    cg->getRange(pasynUser, &polarity);
    cg->getGround(pasynUser, &ground);

    configControl = 0;
    configControl |= pci9116AIChannelGroup::daskPolarity[polarity];
    configControl |= pci9116AIChannelGroup::daskInputMode[inputMode];
    configControl |= pci9116AIChannelGroup::daskGround[ground];

    NDS_DBG("Calling (simple) AI_9116_Config: %u %u %u %u %u %u.",
            _CardNumberReg, configControl, P9116_AI_DMA, 0, 0, 0);
    status = AI_9116_Config(_CardNumberReg, configControl, P9116_AI_DMA, 0, 0, 0);
    if (status != NoError) {
        NDS_ERR("AI_9116_Config error:%s.", pcidask_decode_error(status));
        error();
        cg->unlock();
        return ndsError;
    }

    range = (epicsUInt16)_Range;
    if (polarity == pci9116_polarity_unipolar) {
        range += pci9116_range_1V25;
    }
    range = pci9116AIChannelGroup::daskRange[range];

    NDS_DBG("Calling AI_VReadChannel: %u %u %u.", _CardNumberReg, (U16)getChannelNumber(), range);
    status = AI_VReadChannel(_CardNumberReg, (U16)getChannelNumber(), range, value);
    if (status != NoError) {
        NDS_ERR("AI_VReadChannel error = %s.", pcidask_decode_error(status));
        error();
        cg->unlock();
        return ndsError;
    }

    cg->unlock();

    NDS_DBG("Channel %d read value %f.", getChannelNumber(), *value);

    return ndsSuccess;
}


/**
 * @brief State handler for transition from DISABLED to PROCESSING.
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Successful transition.
 * @retval ndsError Buffer allocation failed.
 *
 * Reallocate voltage buffer for the channel so that it is able to hold the
 * chosen number of samples (as set by the AI channel group).
 */
ndsStatus pci9116AIChannel::onSwitchProcessing(nds::ChannelStates from, nds::ChannelStates to) {
    NDS_TRC("%s", __func__);

    freeBuffers();
    _ChannelBuffer = (epicsFloat32 *)malloc(_SamplesCount * sizeof(epicsFloat32));
    if (!_ChannelBuffer) {
        NDS_ERR("Cannot allocate memory for channel %d data.", getChannelNumber());
        return ndsError;
    }

    NDS_DBG("Channel %d allocated buffer for %d samples.", getChannelNumber(), _SamplesCount);

    return ndsSuccess;
}


/**
 * @brief State handler for transition PROCESSING.    cg->error();
 *
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Successful transition.
 *
 * Check if the acquisition finished successfully. If so then retrieve the
 * compound voltage buffer from the AI channel group and pick the values that
 * correspond to the channel.
 */
ndsStatus pci9116AIChannel::onLeaveProcessing(nds::ChannelStates from, nds::ChannelStates to) {
    pci9116AIChannelGroup   *cg;
    int                     iter, numChannels, position;
    epicsFloat32            *dataBufferV;
    epicsUInt16             *gainQueueChannels;

    NDS_TRC("%s", __func__);

    /* If acquisition didn't finish completely do nothing. */
    cg = dynamic_cast<pci9116AIChannelGroup *>(getChannelGroup());
    if (!cg->isDaqFinished()) {
        NDS_DBG("Daq not completed channel %d skipping buffer update.", getChannelNumber());
        return ndsSuccess;
    }

    dataBufferV = cg->getVoltageBuffer();
    gainQueueChannels = cg->getGainQueueChannels();
    numChannels = cg->getNumChannels();

    position = 0;
    for (iter = 0; iter < numChannels; iter++) {
        if (gainQueueChannels[iter] == getChannelNumber()) {
            position = iter;
            break;
        }
    }

    for (iter = 0; iter < _SamplesCount; iter++, position += numChannels) {
        _ChannelBuffer[iter] = dataBufferV[position];
    }

    doCallbacksFloat32Array(_ChannelBuffer, (size_t)_SamplesCount, _interruptIdBufferFloat32);
    return ndsSuccess;
}


/**
 * @brief State handler for transition to DISABLED.
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Successful transition.
 *
 * After IOC init update the readback records with currently set values.
 */
ndsStatus pci9116AIChannel::onEnterDisabled(nds::ChannelStates from, nds::ChannelStates to) {
    NDS_TRC("%s", __func__);

    /* If we came from IOC_INITIALIZATION update readbacks with values set by PINI records.*/
    if (from == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        doCallbacksInt32(_isEnabled, _interruptIdEnabled);
        doCallbacksInt32(_SamplesCount, _interruptIdSamplesCount);
        doCallbacksInt32(_Range, _interruptIdRange);
    }

    return ndsSuccess;
}


/**
 * @brief State handler for transition to ERROR.
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Successful transition.
 *
 * Put the AI channel group to ERROR state.
 */
ndsStatus pci9116AIChannel::onEnterError(nds::ChannelStates from, nds::ChannelStates to) {
    NDS_TRC("%s", __func__);

    getChannelGroup()->error();

    return ndsSuccess;
}


/**
 * @brief Message handler for START message.
 * @param [in] pasynUser Asyn user struct.
 * @param [in] value Received message (always START in our case).
 *
 * @retval ndsError This is never allowed.
 *
 * Starting an acquisition should only be done through the AI channel group.
 */
ndsStatus pci9116AIChannel::handleStartMsg(asynUser *pasynUser, const nds::Message& value) {
    NDS_ERR("AI channel cannot be started manually.");
    return ndsError;
}


/**
 * @brief Message handler for STOP message.
 * @param [in] pasynUser Asyn user struct.
 * @param [in] value Received message (always STOP in our case).
 *
 * @retval ndsError This is never allowed.
 *
 * Stopping an acquisition should only be done through the AI channel group.
 */
ndsStatus pci9116AIChannel::handleStopMsg(asynUser *pasynUser, const nds::Message& value) {
    NDS_ERR("AI channel cannot be stopped manually.");
    return ndsError;
}


/**
 * @brief Free voltage buffer.
 */
void pci9116AIChannel::freeBuffers() {
    if (_ChannelBuffer) {
        free(_ChannelBuffer);
        _ChannelBuffer = NULL;
    }
}


/**
 * @brief Set device context for the channel.
 * @param [in] newCardNumberReg Device context.
 *
 * @retval ndsSuccess Device context set successfully.
 *
 * Remember the device context.
 */
ndsStatus pci9116AIChannel::setCardNumber(epicsUInt16 newCardNumberReg) {
    _CardNumberReg = newCardNumberReg;
    return ndsSuccess;
}
