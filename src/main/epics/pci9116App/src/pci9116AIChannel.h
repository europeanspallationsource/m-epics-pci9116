/**
 * @file pci9116AIChannel.h
 * @brief Header file defining the cPCI-9116 DAQ card analog input channel class.
 * @author kstrnisa
 * @date 20.8.2013
 */
#ifndef _pci9116AIChannel_h
#define _pci9116AIChannel_h

#include "ndsADIOChannel.h"

/**
 * @brief cPCI-9116 specific nds::ADIOChannel class that supports AI channels.
 */
class pci9116AIChannel: public nds::ADIOChannel {
private:
    epicsUInt16 _CardNumberReg;     /**< Device context for userspace library. */
    epicsFloat32 *_ChannelBuffer;   /**< Buffer for data acquired for each channel. */

public:
    pci9116AIChannel();
    ~pci9116AIChannel();

    virtual ndsStatus getValueFloat64(asynUser* pasynUser, epicsFloat64 *value);
    virtual ndsStatus setEnabled(asynUser* pasynUser, epicsInt32 value);
    virtual ndsStatus setSamplesCount(asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus setRange(asynUser* pasynUser, epicsInt32 value);

    ndsStatus onSwitchProcessing(nds::ChannelStates from, nds::ChannelStates to);
    ndsStatus onLeaveProcessing(nds::ChannelStates from, nds::ChannelStates to);
    ndsStatus onEnterDisabled(nds::ChannelStates from, nds::ChannelStates to);
    ndsStatus onEnterError(nds::ChannelStates from, nds::ChannelStates to);

    ndsStatus setCardNumber(epicsUInt16 newCardNumberReg);

private:
    void freeBuffers();

    virtual ndsStatus handleStartMsg(asynUser *pasynUser, const nds::Message& value);
    virtual ndsStatus handleStopMsg(asynUser *pasynUser, const nds::Message& value);
};

#endif
