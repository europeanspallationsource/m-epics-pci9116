/**
 * Adlink PCI9116 EPICS module
 * Copyright (C) 2015 Cosylab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @file pci9116AIChannelGroup.cpp
 * @brief Implementation of cPci-9116 DAQ card analog input channel group in NDS.
 * @author kstrnisa
 * @date 20.8.2013
 */

#include "ndsChannelGroup.h"
#include "ndsChannelStateMachine.h"
#include "ndsTrigger.h"
#include "ndsTriggerCondition.h"
#include "ndsTaskManager.h"
#include "ndsThreadTask.h"

#include "pci9116Device.h"
#include "pci9116AIChannelGroup.h"
#include "pci9116AIChannel.h"

#include <math.h>
#include <dask.h>
#include "pcidask_errors.h"


const epicsUInt16 pci9116AIChannelGroup::daskRange[8] = {AD_B_5_V, AD_B_2_5_V, AD_B_1_25_V, AD_B_0_625_V, AD_U_10_V, AD_U_5_V, AD_U_2_5_V, AD_U_1_25_V};
const epicsUInt16 pci9116AIChannelGroup::daskTriggerType[4] = {P9116_TRGMOD_SOFT, P9116_TRGMOD_POST, P9116_TRGMOD_PRE, P9116_TRGMOD_MIDL};
const epicsUInt16 pci9116AIChannelGroup::daskClockSource[2] = {P9116_AI_IntTimeBase, P9116_AI_ExtTimeBase};
const epicsUInt16 pci9116AIChannelGroup::daskTrigPolarity[2] = {P9116_AI_TrgPositive, P9116_AI_TrgNegative};
const epicsUInt16 pci9116AIChannelGroup::daskPolarity[2] = {P9116_AI_BiPolar, P9116_AI_UniPolar};
const epicsUInt16 pci9116AIChannelGroup::daskInputMode[2] = {P9116_AI_SingEnded, P9116_AI_Differential};
const epicsUInt16 pci9116AIChannelGroup::daskGround[2] = {P9116_AI_LocalGND, P9116_AI_UserCMMD};


/**
 * @brief AI ChannelGroup constructor.
 * @param [in] name Channel Group name.
 *
 * Register state transition handlers and message handlers. For details
 * refer to NDS documentation.
 */
pci9116AIChannelGroup::pci9116AIChannelGroup(const std::string& name) : nds::ChannelGroup(name) {

    registerOnRequestStateHandler(nds::CHANNEL_STATE_DISABLED, nds::CHANNEL_STATE_PROCESSING,
            boost::bind(&pci9116AIChannelGroup::onSwitchProcessing, this, _1, _2));

    registerOnLeaveStateHandler(nds::CHANNEL_STATE_PROCESSING,
            boost::bind(&pci9116AIChannelGroup::onLeaveProcessing, this, _1, _2));

    registerOnEnterStateHandler(nds::CHANNEL_STATE_DISABLED,
            boost::bind(&pci9116AIChannelGroup::onEnterDisabled, this, _1, _2));

    registerOnEnterStateHandler(nds::CHANNEL_STATE_ERROR,
            boost::bind(&pci9116AIChannelGroup::onEnterError, this, _1, _2));

    _DaqTask = nds::ThreadTask::create(nds::TaskManager::generateName("daqTask"),
            epicsThreadGetStackSize(epicsThreadStackSmall),
            epicsThreadPriorityHigh,
            boost::bind(&pci9116AIChannelGroup::daqTask, this, _1));

    _DataBufferRaw = NULL;
    _DataBufferV = NULL;
};

/**
 * @brief AI ChannelGroup destructor.
 *
 * Free raw and voltage buffers.
 */
pci9116AIChannelGroup::~pci9116AIChannelGroup() {
    freeBuffers();
}


/**
 * @brief Set processing mode.
 * @param [in] pasynUser Asyn user struct.
 * @param [in] value Processing mode to set.
 *
 * @retval ndsSuccess Parameter set successfully.
 * @retval ndsError Parameter invalid.
 *
 * Set the processing mode for the data acquisition. Options are enumerated 
 * in #pci9116_mode. The choice of processing mode determines how 
 * is the number of samples is interpreted, see #setSamplesCount.
 */
ndsStatus pci9116AIChannelGroup::setProcessingMode(asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    if (value < pci9116_mode_finite || value > pci9116_mode_continuous) {
        NDS_ERR("Invalid processing mode.");
        return ndsError;
    }

    NDS_DBG("Setting processing mode %d.", value);

    _ProcessingMode = value;
    doCallbacksInt32(_ProcessingMode, _interruptIdProcessingMode);
    return ndsSuccess;
}


/**
 * @brief Set data acquisition clock source.
 * @param [in] pasynUser Asyn user struct.
 * @param [in] value Data acquisition clock source to set.
 *
 * @retval ndsSuccess Parameter set successfully.
 * @retval ndsError Parameter invalid.
 *
 * Set the source of the clock for data acquisition. Options are enumerated in
 * #pci9116_timebase.
 */
ndsStatus pci9116AIChannelGroup::setClockSource(asynUser* pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    if (value < pci9116_timebase_internal || value > pci9116_timebase_external) {
        NDS_ERR("Invalid clock source.");
        return ndsError;
    }

    NDS_DBG("Setting clock source %d.", value);

    _ClockSource = value;
    doCallbacksInt32(_ClockSource, _interruptIdClockSource);
    return ndsSuccess;
}


/**
 * @brief Set clock frequency.
 * @param [in] pasynUser Asyn user struct.
 * @param [in] value Clock frequency in Hz to set.
 *
 * @retval ndsSuccess Parameter set successfully.
 * @retval ndsError Parameter invalid.
 *
 * Set the frequency of the clock used for data acquisition when internal
 * clock source is selected. In case of an invalid choice the value is rounded to
 * the nearest lower frequency. The high limit for the clock frequency
 * depends on the number of enabled channels since the card does not support
 * simultaneous sampling. Regardless of the number of enabled channels the
 * absolute high limit for the clock frequency is 
 * #PCI9116NDS_INT_CLK_FREQ / #PCI9116NDS_COUNTER_MIN.
 */
ndsStatus pci9116AIChannelGroup::setClockFrequency(asynUser* pasynUser, epicsFloat64 value) {
    ndsStatus   status;
    int         scanCounter, nchannels;

    NDS_TRC("%s", __func__);

    status = ndsSuccess;
    
    /* Protect against non-positive frequency setting by treating it as a 
     * very low frequency. Meaning it will be rounded up to the lowest supported. */
    if (value < 1) {
        value = 1;
    }

    scanCounter = ceil(PCI9116NDS_INT_CLK_FREQ / value);
    nchannels = getEnabledChannels();
    
    if (scanCounter <  PCI9116NDS_COUNTER_MIN) {
        NDS_ERR("Clock frequency too high.");
        scanCounter = PCI9116NDS_COUNTER_MIN;
    }

    if (scanCounter <  PCI9116NDS_COUNTER_MIN * nchannels) {
        NDS_ERR("Clock frequency too high for %d channels enabled.", nchannels);
        scanCounter = PCI9116NDS_COUNTER_MIN * nchannels;
    }

    if (scanCounter > PCI9116NDS_COUNTER_MAX) {
        NDS_ERR("Clock frequency too low.");
        scanCounter = PCI9116NDS_COUNTER_MAX;
    }

    _ClockFrequency = PCI9116NDS_INT_CLK_FREQ / scanCounter;

    if (_ClockFrequency != value) {
        NDS_ERR("Invalid clock frequency.");
        status = ndsError;
    }

    NDS_DBG("Setting clock frequency %f with scan counter %d.", _ClockFrequency, scanCounter);

    doCallbacksFloat64(_ClockFrequency, _interruptIdClockFrequency);
    return status;
}


/**
 * @brief Set clock divisor.
 * @param [in] pasynUser Asyn user struct.
 * @param [in] value Clock divisor to set.
 *
 * @retval ndsSuccess Parameter set successfully.
 * @retval ndsError Parameter invalid.
 *
 * Set the divisor of the clock used for data acquisition when external
 * clock source is selected. The low limit for the clock divisor
 * depends on the number of enabled channels since the card does not support
 * simultaneous sampling. Regardless of the number of enabled channels the
 * absolute low limit for the clock divider is #PCI9116NDS_COUNTER_MIN.
 */
ndsStatus pci9116AIChannelGroup::setClockDivisor(asynUser* pasynUser, epicsInt32 value) {
    ndsStatus   status;
    int         nchannels;

    NDS_TRC("%s", __func__);

    status = ndsSuccess;
    nchannels = getEnabledChannels();
    
    if (value < PCI9116NDS_COUNTER_MIN) {
        NDS_ERR("Clock divisor too low.");
        value = PCI9116NDS_COUNTER_MIN;
        status = ndsError;
    }

    if (value < PCI9116NDS_COUNTER_MIN * nchannels) {
        NDS_ERR("Clock divisor too low for %d channels enabled.", nchannels);
        value = PCI9116NDS_COUNTER_MIN * nchannels;
        status = ndsError;
    }

    if (value > USHRT_MAX) {
        NDS_ERR("Clock divisor too high.");
        value = USHRT_MAX;
        status = ndsError;
    }

    NDS_DBG("Setting clock divisor (scan counter) %d.", value);

    _ClockDivisor = value;
    doCallbacksInt32(_ClockDivisor, _interruptIdClockDivisor);
    return status;
}


/**
 * @brief Set number of samples to acquire.
 * @param [in] pasynUser Asyn user struct.
 * @param [in] value Number of samples to set.
 *
 * @retval ndsSuccess Parameter set successfully.
 * @retval ndsError Parameter invalid.
 *
 * Set the number of samples to acquire for each enabled channel. The number of
 * samples cannot be smaller than the absolute value of trigger delay since that
 * doesn't correspond to any trigger mode of the card.
 */
ndsStatus pci9116AIChannelGroup::setSamplesCount(asynUser *pasynUser, epicsInt32 value) {
    ndsStatus    status;

    NDS_TRC("%s", __func__);

    status = ndsSuccess;

    if (value < 0) {
        NDS_ERR("Number of samples cannot be negative.");
        value = 0;
        status = ndsError;
    }

    if (value < -_TriggerDelay) {
        NDS_ERR("Number of samples cannot be less then absolute value of trigger delay.");
        value = -_TriggerDelay;
        status = ndsError;
    }

    NDS_DBG("Setting number of samples %d.", value);

    _SamplesCount = value;
    doCallbacksInt32(_SamplesCount, _interruptIdSamplesCount);
    return status;
}


/**
 * @brief Trigger an acquisition.
 * @param [in] pasynUser Asyn user struct.
 *
 * @retval ndsSuccess Acquisition started successfully.
 * @retval ndsError Failed to start acquisition.
 *
 * The value parameter is ignored, the flag that signifies that the acquisition
 * was triggered by the user is set and the channel group is put into PROCESSING
 * state.
 *
 * Starting an acquisition is only allowed when the Device is in ON state and
 * the channel group is in DISABLED state.
 */
ndsStatus pci9116AIChannelGroup::setTrigger(asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    if (_device->getCurrentState() != nds::DEVICE_STATE_ON) {
        NDS_ERR("Acquisition can only be started when device is in ON state.");
        return ndsError;
    }

    if (getCurrentState() != nds::CHANNEL_STATE_DISABLED) {
        NDS_ERR("Acquisition can only be started when channel group is in DISABLED state.");
        return ndsError;
    }

    _isTriggered = 1;
    return start();
}


/**
 * @brief Validate the parsed contents of the trigger condition vector.
 * @param [in] pasynUser Asyn user struct.
 * @param [in] trigger Parsed trigger object.
 *
 * @retval ndsSuccess Parameter set successfully.
 * @retval ndsError Invalid trigger specification.
 *
 * The card supports only one trigger channel (external0) and either rising
 * or falling edge.
 */
ndsStatus pci9116AIChannelGroup::onTriggerConditionParsed(asynUser *pasynUser, const nds::Trigger& trigger) {
    nds::ITriggerCondition    *tc;
    nds::Channel            *ch;
    nds::ChannelGroup       *cg;
    Operations              operation;

    NDS_TRC("%s", __func__);

    if (trigger.condition.size() != 1) {
        NDS_ERR("Only one trigger condition at once supported.");
        return ndsError;
    }

    tc = trigger.condition.front();
    ch = tc->getChannel();
    cg = ch->getChannelGroup();
    operation = tc->getOperation();

    if (cg->getName() != PCI9116NDS_EXTCG_NAME) {
        NDS_ERR("Only external trigger line supported as trigger condition.");
        return ndsError;
    }
    if (operation != NDS_TC_OP_NONE
            && operation != NDS_TC_OP_RISING
            && operation != NDS_TC_OP_FALLING) {
        NDS_ERR("Trigger operation %d not supported.", operation);
        return ndsError;
    }
    if (tc->isNegated()) {
        NDS_ERR("Negated trigger condition not supported.");
        return ndsError;
    }
    if (tc->isMinus()) {
        NDS_ERR("Negative trigger condition not supported.");
        return ndsError;
    }

    NDS_DBG("Setting external trigger source with %s edge.",
            operation == NDS_TC_OP_FALLING ? "falling" : "rising");

    return ndsSuccess;
}


/**
 * @brief Set trigger delay.
 * @param [in] pasynUser Asyn user struct.
 * @param [in] value Trigger delay in number of samples.
 *
 * @retval ndsSuccess Parameter set successfully.
 * @retval ndsError Parameter invalid.
 *
 * Setting trigger delay determines the actual trigger mode of the card when
 * acquisition is started.
 *
 * If trigger delay is zero (all samples are to be acquired after the external
 * trigger) than the trigger mode will be post-trigger.
 *
 * If trigger delay is equal to the number of samples (all samples are to be
 * acquired before the external trigger) than the trigger mode will be pre-trigger.
 *
 * Otherwise (trigger delay samples are to be acquired before the external trigger
 * and the rest after) the trigger mode will be middle-trigger.
 */
ndsStatus pci9116AIChannelGroup::setTriggerDelay(asynUser* pasynUser, epicsInt32 value) {
    ndsStatus status;

    NDS_TRC("%s", __func__);

    status = ndsSuccess;

    if (value > 0) {
        NDS_ERR("Only non-positive trigger delay supported.");
        value = 0;
        status = ndsError;
    }

    if (-value > _SamplesCount) {
        NDS_ERR("Absolute value of trigger delay must be smaller than number of samples.");
        value = -_SamplesCount;
        status = ndsError;
    }

    NDS_DBG("Setting trigger delay %d.", value);

    _TriggerDelay = value;
    doCallbacksInt32(_TriggerDelay, _interruptIdTriggerDelay);
    return status;
}


/**
 * @brief Set number of triggers the card will accept in a single acquisition.
 * @param [in] pasynUser Asyn user struct.
 * @param [in] value Number of retriggers.
 *
 * @retval ndsSuccess Parameter set successfully.
 * @retval ndsError Parameter invalid.
 *
 * Setting the trigger repeat to more than one will multiply the number of samples
 * actually acquired for each channel. Setting trigger repeat to a negative value
 * means that the card will accept an inifite amount of external triggers and
 * update buffers after each acquisition (and automatically start waiting for
 * next trigger). Thus inifinite trigger repeat doesn't increase the number of
 * samples that will be acquired for each channel.
 */
ndsStatus pci9116AIChannelGroup::setTriggerRepeat(asynUser* pasynUser, epicsInt32 value) {
    ndsStatus    status;

    NDS_TRC("%s", __func__);

    status = ndsSuccess;

    if (value > USHRT_MAX) {
        value = USHRT_MAX;
        status = ndsError;
    }

    if (status != ndsSuccess) {
        NDS_ERR("Trigger repeat out of bounds.");
    }

    NDS_DBG("Setting trigger repeat %d.", value);

    _TriggerRepeat = value;
    doCallbacksInt32(_TriggerRepeat, _interruptIdTriggerRepeat);
    return status;
}


/**
 * @brief Set polarity for data acquisition.
 * @param [in] pasynUser Asyn user struct.
 * @param [in] polarity Polarity to set.
 *
 * @retval ndsSuccess Parameter set successfully.
 * @retval ndsError Parameter invalid.
 *
 * Set polarity for data acquisition. Possible values are enumerated in
 * #pci9116_polarity.
 */
ndsStatus pci9116AIChannelGroup::setRange(asynUser* pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    if (value < pci9116_polarity_bipolar || value > pci9116_polarity_unipolar) {
        NDS_ERR("Invalid range/polarity.");
        return ndsError;
    }

    NDS_DBG("Setting polarity %d.", value);

    _Range = value;
    doCallbacksInt32(_Range, _interruptIdRange);
    return ndsSuccess;
}


/**
 * @brief Set the input mode of analog channels.
 * @param [in] pasynUser Asyn user struct.
 * @param [in] diff Channel Input mode to set.
 *
 * @retval ndsSuccess Parameter set successfully.
 * @retval ndsError Parameter invalid or operation not permitted.
 *
 * Set the input mode for analog channels. Possible values are enumerated in
 * #pci9116_input_mode. Switching to differential mode is allowed only if no
 * channels > 31 are enabled.
 */
ndsStatus pci9116AIChannelGroup::setDifferential(asynUser* pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    if (value < pci9116_input_mode_single || value > pci9116_input_mode_differential) {
        NDS_ERR("Invalid input mode.");
        return ndsError;
    }

    if (value == pci9116_input_mode_differential) {
        for(ChannelContainer::iterator iter = _nodes.begin(); iter != _nodes.end(); ++iter) {
            if (iter->second->isEnabled() &&
                    iter->second->getChannelNumber() >= PCI9116NDS_NUM_AI_CHANNELS / 2) {
                NDS_ERR("Cannot switch to differential if channel %d is enabled.", iter->second->getChannelNumber());
                return ndsError;
            }
        }
    }

    NDS_DBG("Setting input mode %d.", value);

    _Differential = value;
    doCallbacksInt32(_Differential, _interruptIdDifferential);
    return ndsSuccess;
}


/**
 * @brief Set grounding source.
 * @param [in] pasynUser Asyn user struct.
 * @param [in] ground Grounding source to set.
 *
 * @retval ndsSuccess Parameter set successfully.
 * @retval ndsError Parameter invalid.
 *
 * Set the grounding source for analog inputs Possible values are enumerated in
 * #pci9116_ground.
 */
ndsStatus pci9116AIChannelGroup::setGround(asynUser* pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    if (value < pci9116_ground_local || value > pci9116_ground_user) {
        NDS_ERR("Invalid grounding source.");
        return ndsError;
    }

    NDS_DBG("Setting grounding source %d.", value);

    _Ground = value;
    doCallbacksInt32(_Ground, _interruptIdGround);
    return ndsSuccess;
}


/**
 * @brief State handler for transition from DISABLED to PROCESSING.
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Successful transition.
 * @retval ndsBlockTransition No enabled channels or zero number of samples.
 * @retval ndsError Invalid acquisition parameters.
 *
 * During this transition the NDS driver parameters are translated to the
 * parameters of the card and the card is configured.
 *
 * The gain queue is constructed from the list of channels that are enabled.
 * Each channel is added to the gain queue once and the channels are
 * sorted by their indices in ascending order.
 *
 * The trigger mode is obtained based on the value of trigger delay and number
 * of samples. The trigger polarity is obtained from the currently selected
 * trigger condition.
 *
 * The analog input configuration (polarity, input mode and grounding source)
 * and trigger configuration (trigger type, trigger polarity and clock source)
 * is combined into the compound configuration parameters (used to configure
 * the card through the userspace library).
 *
 * The buffer size is calculated based on the chosen trigger type and processing mode.
 */
ndsStatus pci9116AIChannelGroup::onSwitchProcessing(nds::ChannelStates from, nds::ChannelStates to) {
    int         status;
    epicsUInt16    midCnt, postCnt, trgCnt;
    epicsUInt32    scanCnt, sampCnt;
    epicsInt32    range;

    NDS_TRC("%s", __func__);

    midCnt = 0;
    postCnt = 0;
    trgCnt = 0;
    scanCnt = (epicsUInt32)_ClockDivisor;
    sampCnt = PCI9116NDS_COUNTER_MIN;

    _ConfigControl = 0;
    _ConfigControl |= daskPolarity[_Range];
    _ConfigControl |= daskInputMode[_Differential];
    _ConfigControl |= daskGround[_Ground];

    _TriggerControl = 0;
    _TriggerControl |= P9116_AI_DMA;
    _TriggerControl |= daskClockSource[_ClockSource];

    /* If there are no samples to acquire there will be nothing to do and we
     * might as well stay disabled. */
    if (!_SamplesCount) {
        NDS_WRN("Number of samples is zero, doing nothing.");
        return ndsBlockTransition;
    }

    /* Iterate over channels and include enabled channels in acquisition. */
    _NumChannels = 0;
    for(ChannelContainer::iterator iter = _nodes.begin(); iter != _nodes.end(); ++iter) {
        if (iter->second->isEnabled()) {
            iter->second->getRange(NULL, &range);
            if (_Range == pci9116_polarity_unipolar) {
                range += pci9116_range_1V25 + 1;
            }

            _GainQueueChannels[_NumChannels] = (epicsUInt16)iter->second->getChannelNumber();
            _GainQueueRanges[_NumChannels] = daskRange[range];
            NDS_DBG("GQ element %d: ch %u rng %u.", _NumChannels, _GainQueueChannels[_NumChannels], _GainQueueRanges[_NumChannels]);

            _NumChannels++;
        }
    }

    /* If there are no enabled channels there will be nothing to do and we
     * might as well stay disabled. */
    if (!_NumChannels) {
        NDS_WRN("No enabled channels, doing nothing.");
        return ndsBlockTransition;
    }

    if (!_isTriggered) {
        if (_trigger->condition.front()->getOperation() == NDS_TC_OP_FALLING) {
            _TriggerControl |= P9116_AI_TrgNegative;
        } else {
            _TriggerControl |= P9116_AI_TrgPositive;
        }
    } else {
        _TriggerControl |= P9116_TRGMOD_SOFT;
    }

    if (_ProcessingMode == pci9116_mode_finite) {
        _DaqMode = pci9116_mode_finite;
        _ReadCount = (epicsUInt32)_SamplesCount;

        if (!_isTriggered) {
            if (!_TriggerDelay) {
                if (_TriggerRepeat > 1) {
                    trgCnt = (epicsUInt16)_TriggerRepeat;
                    _TriggerControl |= P9116_AI_ReTrigEn;
                    _ReadCount *= (epicsUInt32)trgCnt;
                }
                _TriggerControl |= P9116_TRGMOD_POST;
            } else if (-_TriggerDelay < _SamplesCount) {
                midCnt = (epicsUInt16)-_TriggerDelay;
                postCnt = (epicsUInt16)(_SamplesCount + _TriggerDelay);
                _TriggerControl |= P9116_TRGMOD_MIDL;
                _TriggerControl |= P9116_AI_MCounterEn;
            } else if (-_TriggerDelay == _SamplesCount) {
                midCnt = (epicsUInt16)_SamplesCount;
                _TriggerControl |= P9116_TRGMOD_PRE;
                _TriggerControl |= P9116_AI_MCounterEn;
            }
        }

        /* oddReadCount checks if number of acquired data is odd (the driver doesn't finish reading in
         * such case). This variable is used to add additional read count and it all works OK,
         * except in case of re-triggering, in this case an error is reported. */
        _OddReadCount = (_ReadCount * _NumChannels) % 2;

        if (trgCnt && _OddReadCount) {
            NDS_ERR("In re-trigger mode number of scans must be even.");
            doCallbacksMessage("ERROR", ndsError, "In re-trigger mode number of scans must be even.");
            return ndsError;
        }

        _BufferSize = (_ReadCount + _OddReadCount) * _NumChannels;
    } else {
        _DaqMode = pci9116_mode_continuous;

        if (!_isTriggered) {
            _TriggerControl |= P9116_TRGMOD_POST;
        }

        if (_SamplesCount % 2 != 0) {
            NDS_ERR("In continuous mode scan repeat must be even.");
            doCallbacksMessage("ERROR", ndsError, "In continuous mode scan repeat must be even.");
            return ndsError;
        }

        /* In double buffer mode _ReadCount determines number of samples
         * per channel in circular buffer. It must be a multiple of four. */
        _ReadCount = _SamplesCount * 2;
        _HalfCount = _SamplesCount * _NumChannels;
        _BufferSize = _HalfCount;
    }

    /* Set channels _SamplesCount so they know how much data must they put
     * into their record buffers while taking re-triggers into account. */
    for(ChannelContainer::iterator iter = _nodes.begin(); iter != _nodes.end(); ++iter) {
        if (iter->second->isEnabled()) {
            iter->second->setSamplesCount(NULL, _SamplesCount * (trgCnt > 1 ? trgCnt : 1));
        }
    }

    /* Check if _BufferSize > available memory although it should never happen
     * due to other checks when settings are made. */
    NDS_DBG("readCount %u, numChans %u, bufferSize %u.", _ReadCount, _NumChannels, _BufferSize);
    if (_BufferSize * (int)sizeof(epicsUInt16) > (dynamic_cast<pci9116Device *>(_device))->getMemorySize()) {
        NDS_ERR("Not enough memory reserved on card.");
        doCallbacksMessage("ERROR", ndsError, "Not enough memory reserved on card.");
        return ndsError;
    }

    freeBuffers();

    /* Reallocate raw buffer. */
    _DataBufferRaw = (epicsUInt16 *)malloc(_BufferSize * sizeof(epicsUInt16));
    if (!_DataBufferRaw) {
        NDS_ERR("Not enough memory, number of samples should be smaller.");
        return ndsError;
    }
    NDS_DBG("Allocated raw buffer size %u.", _BufferSize);

    /* Reallocate voltage buffer. */
    _DataBufferV = (epicsFloat32 *)malloc(_BufferSize * sizeof(epicsFloat32));
    if (!_DataBufferV) {
        NDS_ERR("Not enough memory, number of samples should be smaller.");
        return ndsError;
    }
    NDS_DBG("Allocated voltage buffer size %u.", _BufferSize);

    NDS_DBG("Calling AI_9116_Config: %u 0x%x 0x%x %u %u %u.",
            _CardNumberReg, _ConfigControl, _TriggerControl, postCnt, midCnt, trgCnt);
    status = AI_9116_Config(_CardNumberReg, _ConfigControl, _TriggerControl, postCnt, midCnt, trgCnt);
    if (status != NoError) {
        NDS_ERR("AI_9116_Config error: %s.", pcidask_decode_error(status));
        doCallbacksMessage("ERROR", ndsError, "AI_9116_Config error.");
        return ndsError;
    }

    /* Sampling and scanning intervals can't overlap although it should never happen
     * due to other checks when settings are made. */
    if (_ClockSource == pci9116_timebase_internal) {
        scanCnt = (epicsUInt32)floor(PCI9116NDS_INT_CLK_FREQ / _ClockFrequency);
    }

    NDS_DBG("Calling AI_9116_CounterInterval: %u %u %u.", _CardNumberReg, scanCnt, sampCnt);
    status = AI_9116_CounterInterval(_CardNumberReg, scanCnt, sampCnt);
    if (status != NoError) {
        NDS_ERR("AI_9116_CounterInterval error: %s.", pcidask_decode_error(status));
        doCallbacksMessage("ERROR", ndsError, "AI_9116_CounterInterval error.");
        return ndsError;
    }

    _DaqFinished = 0;
    _DaqTask->start();

    return ndsSuccess;
}


/**
 * @brief State handler for transition from PROCESSING.
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Successful transition.
 *
 * The DaqTask is canceled (which will stop continuous acquisition) and the card
 * is reset. If the processing mode was continuous the card's double buffer is
 * checked for any additional contents.
 */
ndsStatus pci9116AIChannelGroup::onLeaveProcessing(nds::ChannelStates from, nds::ChannelStates to) {
    int        status;
    U32        count;
    BOOLEAN fStop, halfReady;

    NDS_TRC("%s", __func__);

    status = AI_AsyncClear(_CardNumberReg, &count);
    if (status != NoError) {
        NDS_ERR("AI_AsyncClear = %s.", pcidask_decode_error(status));
        return ndsError;
    }
    NDS_DBG("AI_AsyncClear returned with: %lu.", count);

    if (_DaqMode == pci9116_mode_continuous && _DaqFinished) {
        /* In continuous mode we already updated buffers during acquisition. */
        _DaqFinished = 0;

        /* Is there anything left in the buffer? */
        status = AI_AsyncDblBufferHalfReady(_CardNumberReg, &halfReady, &fStop);
        if (status != NoError) {
            NDS_ERR("AI_AsyncDblBufferHalfReady = %s.", pcidask_decode_error(status));
            return ndsError;
        }
        NDS_DBG("AI_AsyncDblBufferHalfReady returned with: %u %u.",
                (unsigned)halfReady, (unsigned)fStop);

        if (!halfReady && (count == _HalfCount || !count)) {
        } else {
            if (!count) {
                count = _HalfCount;
            }
            NDS_DBG("Calling AI_AsyncDblBufferTransfer: %u %p.", _CardNumberReg, _DataBufferRaw);
            status = AI_AsyncDblBufferTransfer(_CardNumberReg, _DataBufferRaw);
            if (status != NoError) {
                NDS_ERR("AI_AsyncDblBufferTransfer = %s.", pcidask_decode_error(status));
                return ndsError;
            }

            /* But if there is something left in the buffer we want to update again. */
            _DaqFinished = 1;
        }
    }

    if (_DaqFinished) {
        convertData();
    }

    return ndsSuccess;
}


/**
 * @brief State handler for transition to DISABLED.
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Successful transition.
 *
 * After IOC init update the readback records with currently set values. if the
 * trigger repeat is set to infinite than immediately restart the acquisition.
 */
ndsStatus pci9116AIChannelGroup::onEnterDisabled(nds::ChannelStates from, nds::ChannelStates to) {
    size_t  nbytesTransferead;

    NDS_TRC("%s", __func__);

    /* If we came from PROCESSING and trigger repeat is negative and
     * trigger mode is post and mode is finite then go back to processing. */
    if (from == nds::CHANNEL_STATE_PROCESSING &&
            _TriggerRepeat < 0 &&
            !_isTriggered &&
            _DaqMode == pci9116_mode_finite) {
        start();
    }

    /* If we came from IOC_INITIALIZATION update readbacks with values set by PINI records.*/
    if (from == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        doCallbacksInt32(_isEnabled, _interruptIdEnabled);
        doCallbacksInt32(_ProcessingMode, _interruptIdProcessingMode);
        doCallbacksInt32(_ClockSource, _interruptIdClockSource);
        doCallbacksFloat64(_ClockFrequency, _interruptIdClockFrequency);
        doCallbacksInt32(_ClockDivisor, _interruptIdClockDivisor);
        doCallbacksInt32(_SamplesCount, _interruptIdSamplesCount);
        doCallbacksInt32(_TriggerDelay, _interruptIdTriggerDelay);
        doCallbacksInt32(_TriggerRepeat, _interruptIdTriggerRepeat);
        doCallbacksInt32(_Range, _interruptIdRange);
        doCallbacksInt32(_Differential, _interruptIdDifferential);
        doCallbacksInt32(_Ground, _interruptIdGround);

        /* Set default external trigger line (and the only option really). */
        setTriggerCondition(NULL, "external0", 10, &nbytesTransferead);
    }

    return ndsSuccess;
}


/**
 * @brief State handler for transition to ERROR.
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Successful transition.
 *
 * Put the device to ERROR state.
 */
ndsStatus pci9116AIChannelGroup::onEnterError(nds::ChannelStates from, nds::ChannelStates to) {
    NDS_TRC("%s", __func__);

    _device->error();

    return ndsSuccess;
}


/**
 * @brief Message handler for START message.
 * @param [in] pasynUser Asyn user struct.
 * @param [in] value Received message (always START in our case).
 *
 * @retval ndsSuccess Acquisition started successfully.
 * @retval ndsError Failed to start acquisition.
 *
 * Starting an acquisition is only allowed when the Device is in ON state and
 * the channel group is in DISABLED state.
 */
ndsStatus pci9116AIChannelGroup::handleStartMsg(asynUser *pasynUser, const nds::Message& value) {
    NDS_TRC("%s", __func__);

    if (_device->getCurrentState() != nds::DEVICE_STATE_ON) {
        NDS_ERR("Acquisition can only be started when device is in ON state.");
        return ndsError;
    }

    if (getCurrentState() != nds::CHANNEL_STATE_DISABLED) {
        NDS_ERR("Acquisition can only be started when channel group is in DISABLED state.");
        return ndsError;
    }

    _isTriggered = 0;
    return start();
}


/**
 * @brief Message handler for STOP message.
 * @param [in] pasynUser Asyn user struct.
 * @param [in] value Received message (always STOP in our case).
 *
 * @retval ndsSuccess Acquisition started successfully.
 * @retval ndsError Failed to stop acquisition.
 *
 * Stopping an acquisition is only allowed when the channel group is in
 * PROCESSING state.
 */
ndsStatus pci9116AIChannelGroup::handleStopMsg(asynUser *pasynUser, const nds::Message& value) {
    NDS_TRC("%s", __func__);

    if (getCurrentState() != nds::CHANNEL_STATE_PROCESSING) {
        NDS_ERR("Acquisition can only be stopped when channel group is in PROCESSING state.");
        return ndsError;
    }
    
    _isTriggered = 1;
    return stop();
}


ndsStatus pci9116AIChannelGroup::daqTask(nds::TaskServiceBase &service) {
    int             status;
    epicsFloat64    DAQSleepTime;
    U32                count;
    BOOLEAN            fStop, halfReady;

    NDS_TRC("%s", __func__);

    DAQSleepTime = (1 + _BufferSize / PCI9116NDS_DAQ_SLEEP_FACTOR) *
            PCI9116NDS_DAQ_SLEEP_TIME;
    count = 0;
    fStop = FALSE;
    halfReady = FALSE;

    if (_DaqMode == pci9116_mode_finite) {
        NDS_DBG("Calling AI_AsyncDblBufferMode: %u %u.", _CardNumberReg, (unsigned)FALSE);
        status = AI_AsyncDblBufferMode(_CardNumberReg, FALSE);
        if (status != NoError) {
            NDS_ERR("AI_DblBufferMode Error = %s\n", pcidask_decode_error(status));
            PCI9116NDS_LOCKED_ERROR;
        }

        NDS_DBG("Calling AI_ContReadMultiChannels: %u %u %p %p %p %u %u %u.",
                _CardNumberReg, _NumChannels, _GainQueueChannels, _GainQueueRanges,
                _DataBufferRaw, _ReadCount + _OddReadCount, 1, ASYNCH_OP);
        status = AI_ContReadMultiChannels(
                _CardNumberReg, (U16)_NumChannels, _GainQueueChannels, _GainQueueRanges,
                _DataBufferRaw, _ReadCount + _OddReadCount, 1, ASYNCH_OP);
        if (status != NoError) {
            NDS_ERR("AI_ContReadMultiChannels Error = %s.", pcidask_decode_error(status));
            PCI9116NDS_LOCKED_ERROR;
        }

        NDS_DBG("Calling AI_AsyncCheck in loop, DAQSleepTime = %f", DAQSleepTime);
        do {
            status = AI_AsyncCheck(_CardNumberReg, &fStop, &count);
            if (status != NoError) {
                NDS_ERR("AI_AsyncCheck = %s.", pcidask_decode_error(status));
                PCI9116NDS_LOCKED_ERROR;
            }
            service.sleep(DAQSleepTime);
        } while (!fStop);
        NDS_DBG("AI_AsyncCheck returned with: %u %lu.", (unsigned)fStop, count);

    } else {
        NDS_DBG("Calling AI_AsyncDblBufferMode: %u %u.", _CardNumberReg, (unsigned)TRUE);
        status = AI_AsyncDblBufferMode(_CardNumberReg, TRUE);
        if (status != NoError) {
            NDS_ERR("AI_DblBufferMode error = %s.", pcidask_decode_error(status));
            PCI9116NDS_LOCKED_ERROR;
        }

        NDS_DBG("Calling AI_ContReadMultiChannels: %u %u %p %p %p %u %u %u.",
                _CardNumberReg, _NumChannels, _GainQueueChannels, _GainQueueRanges,
                _DataBufferRaw, _ReadCount, 1, ASYNCH_OP);
        status = AI_ContReadMultiChannels(
                _CardNumberReg, _NumChannels, _GainQueueChannels, _GainQueueRanges,
                _DataBufferRaw, _ReadCount, 1, ASYNCH_OP);
        if (status != NoError) {
            NDS_ERR("AI_ContReadMultiChannels Error = %s.", pcidask_decode_error(status));
            PCI9116NDS_LOCKED_ERROR;
        }

        do {
            NDS_DBG("Calling AI_AsyncDblBufferHalfReady in loop.");
            do {
                status = AI_AsyncDblBufferHalfReady(_CardNumberReg, &halfReady, &fStop);
                if (status != NoError) {
                    NDS_ERR("AI_AsyncDblBufferHalfReady = %s.", pcidask_decode_error(status));
                    PCI9116NDS_LOCKED_ERROR;
                }
                service.sleep(DAQSleepTime);
            } while (!halfReady && !fStop);
            NDS_DBG("AI_AsyncDblBufferHalfReady returned with: %u %u.",
                    (unsigned)halfReady, (unsigned)fStop);

            /* On each iteration check if the acquisition has been canceled. */
            lock();
            if (getCurrentState() != nds::CHANNEL_STATE_PROCESSING) {
                NDS_DBG("Continuous acquisition canceled.");
                unlock();
                return ndsSuccess;
            }

            NDS_DBG("Calling AI_AsyncDblBufferTransfer: %u %p.", _CardNumberReg, _DataBufferRaw);
            status = AI_AsyncDblBufferTransfer(_CardNumberReg, _DataBufferRaw);
            if (status != NoError) {
                NDS_ERR("AI_AsyncDblBufferTransfer = %s.", pcidask_decode_error(status));
                _DaqTask->setState(nds::ndsThreadStopped);
                error();
                unlock();
                return ndsError;
            }

            /* Here we have another batch of data for channels so we need
             * to convert, and make channels update their records. */
            NDS_DBG("Updating buffers in continuous mode.");
            _DaqFinished = 1;
            convertData();
            for(ChannelContainer::iterator iter = _nodes.begin(); iter != _nodes.end(); ++iter) {
                if (iter->second->isEnabled()) {
                    (dynamic_cast<pci9116AIChannel *>(iter->second))->onLeaveProcessing(
                            nds::CHANNEL_STATE_PROCESSING, nds::CHANNEL_STATE_PROCESSING);
                }
            }
            _DaqFinished = 0;

            unlock();
        } while (1);
    }

    lock();
    _DaqFinished = 1;
    _DaqTask->setState(nds::ndsThreadStopped);
    stop();
    unlock();
    return ndsSuccess;
}


/**
 * @brief Converts raw data buffer to voltage buffer.
 *
 * @retval ndsSuccess Conversion successful.
 * @retval ndsError AI_VoltScale from pcidask failed.
 */
ndsStatus pci9116AIChannelGroup::convertData() {
    int status, iter;
    F64    scaledValue;

    NDS_TRC("%s", __func__);
    
    if (!_DataBufferRaw || !_DataBufferV) {
        NDS_WRN("Buffers not allocated.");
        return ndsError;
    }

    for (iter = 0; iter < _BufferSize; iter++) {
        status = AI_VoltScale(_CardNumberReg, _GainQueueRanges[iter%_NumChannels],
                _DataBufferRaw[iter], &scaledValue);
        _DataBufferV[iter] = (epicsFloat32)scaledValue;
        if (status != NoError) {
            NDS_ERR("AI_VoltScale = %s.", pcidask_decode_error(status));
            return ndsError;
        }
    }

    return ndsSuccess;
}


/**
 * @brief Free raw and voltage buffers.
 */
void pci9116AIChannelGroup::freeBuffers() {
    if (_DataBufferRaw) {
        free(_DataBufferRaw);
        _DataBufferRaw = NULL;
    }
    if (_DataBufferV) {
        free(_DataBufferV);
        _DataBufferV = NULL;
    }
}


/**
 * @brief Set device context for the channel group.
 * @param [in] newCardNumberReg Device context.
 *
 * @retval ndsSuccess Device context set successfully.
 *
 * Remember the device context and push the device context to all the channels.
 */
ndsStatus pci9116AIChannelGroup::setCardNumber(epicsUInt16 newCardNumberReg) {
    _CardNumberReg = newCardNumberReg;
    for(ChannelContainer::iterator iter = _nodes.begin(); iter != _nodes.end(); ++iter) {
        (dynamic_cast<pci9116AIChannel *>(iter->second))->setCardNumber(newCardNumberReg);
    }
    return ndsSuccess;
}


epicsFloat32 *pci9116AIChannelGroup::getVoltageBuffer() {
    return _DataBufferV;
}


epicsUInt16 *pci9116AIChannelGroup::getGainQueueChannels() {
    return _GainQueueChannels;
}


int pci9116AIChannelGroup::getNumChannels() {
    return _NumChannels;
}


int pci9116AIChannelGroup::isDaqFinished() {
    return _DaqFinished;
}


int pci9116AIChannelGroup::getEnabledChannels() {
    int nchannels;

    nchannels = 0;
    for(ChannelContainer::iterator iter = _nodes.begin(); iter != _nodes.end(); ++iter) {
        if (iter->second->isEnabled()) {
            nchannels++;
        }
    }

    return nchannels;
}
