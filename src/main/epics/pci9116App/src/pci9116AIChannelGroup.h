/**
 * @file pci9116AIChannelGroup.h
 * @brief Header file defining the cPCI-9116 DAQ card analog input channel group class.
 * @author kstrnisa
 * @date 20.8.2013
 */
#ifndef _pci9116AIChannelGroup_h
#define _pci9116AIChannelGroup_h

#include "ndsChannelGroup.h"
#include "ndsTaskManager.h"
#include "ndsThreadTask.h"


/**
 * @brief cPCI-9116 specific nds::ChannelGroup class that support common AI functionality.
 */
class pci9116AIChannelGroup : public nds::ChannelGroup {
private:
    epicsUInt16 _CardNumberReg;     /**< Device context for userspace library. */
    epicsUInt16 _ConfigControl;     /**< Merged settings for AD control */
    epicsUInt16 _TriggerControl;    /**< Merged settings for Trigger control */
    epicsUInt32 _ReadCount;         /**< In finite mode it means total number of scans to be performed. In continuous mode is number of samples allocated for each channel in the circular buffer and its value must be a multiple of 4. */
    epicsUInt32 _HalfCount;         /**< Size of the half of the circular buffer in double buffered mode */
    epicsUInt16 _OddReadCount;      /**< Needed in special case when numChans*ReadCount is odd and triggering is soft or post. */
    int _BufferSize;                /**< Number of elements in buffer (not bytes). */
    int _NumChannels;               /**< Number of channels in acquisition. */
    int _DaqMode;                   /**< Finite or continuous. */

    nds::ThreadTask *_DaqTask;      /**< NDS task/thread that waits for acquisition to finish. */
    int _DaqFinished;               /**< Flag signifying that the acquisition finished completely. >*/

    epicsUInt16 *_DataBufferRaw;    /**< Raw data buffer, holds all the data from data acquisition. */
    epicsFloat32 *_DataBufferV;     /**< Voltage data buffer, converted from _DataBufferRaw. */

    epicsUInt16 _GainQueueChannels[PCI9116NDS_GAIN_QUEUE_SIZE]; /**< Array of channel indexes. Channels will be sampled in this order on every scan. */
    epicsUInt16 _GainQueueRanges[PCI9116NDS_GAIN_QUEUE_SIZE];   /**< Holds AD range for corresponding channel in Chans array. */

    ndsStatus convertData();

public:
    pci9116AIChannelGroup(const std::string& name);
    ~pci9116AIChannelGroup();

    virtual ndsStatus setProcessingMode(asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus setClockSource(asynUser* pasynUser, epicsInt32 value);
    virtual ndsStatus setClockFrequency(asynUser* pasynUser, epicsFloat64 value);
    virtual ndsStatus setClockDivisor(asynUser* pasynUser, epicsInt32 value);
    virtual ndsStatus setSamplesCount(asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus setTrigger(asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus onTriggerConditionParsed(asynUser *pasynUser, const nds::Trigger& trigger);
    virtual ndsStatus setTriggerDelay(asynUser* pasynUser, epicsInt32 value);
    virtual ndsStatus setTriggerRepeat(asynUser* pasynUser, epicsInt32 value);
    virtual ndsStatus setRange(asynUser* pasynUser, epicsInt32 value);
    virtual ndsStatus setDifferential(asynUser* pasynUser, epicsInt32 value);
    virtual ndsStatus setGround(asynUser* pasynUser, epicsInt32 value);

    ndsStatus setCardNumber(epicsUInt16 newCardNumberReg);
    epicsUInt16 *getGainQueueChannels();
    epicsFloat32 *getVoltageBuffer();
    int getNumChannels();
    int isDaqFinished();
    int getEnabledChannels();

    static const epicsUInt16 daskRange[8];          /**< Translation between NDS and pcidask macros for analog input range. */
    static const epicsUInt16 daskTriggerType[4];    /**< Translation between NDS and pcidask macros for daq trigger type. */
    static const epicsUInt16 daskClockSource[2];    /**< Translation between NDS and pcidask macros for daq clock source. */
    static const epicsUInt16 daskTrigPolarity[2];   /**< Translation between NDS and pcidask macros for daq trigger polarity. */
    static const epicsUInt16 daskPolarity[2];       /**< Translation between NDS and pcidask macros for analog input polarity. */
    static const epicsUInt16 daskInputMode[2];      /**< Translation between NDS and pcidask macros for analog input mode. */
    static const epicsUInt16 daskGround[2];         /**< Translation between NDS and pcidask macros for daq ground source. */

private:
    ndsStatus onSwitchProcessing(nds::ChannelStates from, nds::ChannelStates to);
    ndsStatus onLeaveProcessing(nds::ChannelStates from, nds::ChannelStates to);
    ndsStatus onEnterDisabled(nds::ChannelStates from, nds::ChannelStates to);
    ndsStatus onEnterError(nds::ChannelStates from, nds::ChannelStates to);

    virtual ndsStatus handleStartMsg(asynUser *pasynUser, const nds::Message& value);
    virtual ndsStatus handleStopMsg(asynUser *pasynUser, const nds::Message& value);

    void freeBuffers();
    ndsStatus daqTask(nds::TaskServiceBase &service);
};


#endif
