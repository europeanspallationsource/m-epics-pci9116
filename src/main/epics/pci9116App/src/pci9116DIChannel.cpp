/**
 * Adlink PCI9116 EPICS module
 * Copyright (C) 2015 Cosylab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @file pci9116DIChannel.cpp
 * @brief Implementation of cPCI-9116 DAQ card digital input channel in NDS.
 * @author kstrnisa
 * @date 20.8.2013
 */

#include "pci9116DIChannel.h"
#include "pci9116DIChannelGroup.h"
#include "pci9116Device.h"

#include "ndsADIOChannel.h"

#include <dask.h>
#include "pcidask_errors.h"


/**
 * @brief DI channel constructor.
 */
pci9116DIChannel::pci9116DIChannel() {

    registerOnEnterStateHandler(nds::CHANNEL_STATE_ERROR,
            boost::bind(&pci9116DIChannel::onEnterError, this, _1, _2));
}


/**
 * @brief Get DI channel value.
 * @param [in] pasynUser Asyn user struct.
 * @param [out] value DI channel value.
 */
ndsStatus pci9116DIChannel::getValueInt32(asynUser* pasynUser, epicsInt32 *value) {
    U16 chan_data;
    int status;

    NDS_TRC("%s", __func__);

    status = DI_ReadLine(_CardNumberReg, 0, (U16)getChannelNumber(), &chan_data);
    if (status != NoError) {
        NDS_ERR("DI_ReadLine error = %s.", pcidask_decode_error(status));
        error();
        return ndsError;
    }
    NDS_DBG("DI_ReadLine returned with: %u.", chan_data);

    *value = (epicsInt32)chan_data;
    return ndsSuccess;
}


/**
 * @brief State handler for transition to ERROR.
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Successful transition.
 *
 * Put the DI channel group to ERROR state.
 */
ndsStatus pci9116DIChannel::onEnterError(nds::ChannelStates from, nds::ChannelStates to) {
    pci9116DIChannelGroup   *cg;

    NDS_TRC("%s", __func__);

    cg = dynamic_cast<pci9116DIChannelGroup *>(getChannelGroup());

    cg->lock();
    cg->error();
    cg->unlock();

    return ndsSuccess;
}


/**
 * @brief Set device context for the channel.
 * @param [in] newCardNumberReg Device context.
 *
 * @retval ndsSuccess Device context set successfully.
 *
 * Remember the device context.
 */
ndsStatus pci9116DIChannel::setCardNumber(epicsUInt16 newCardNumberReg) {
    _CardNumberReg = newCardNumberReg;
    return ndsSuccess;
}
