/**
 * Adlink PCI9116 EPICS module
 * Copyright (C) 2015 Cosylab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @file pci9116DIChannelGroup.cpp
 * @brief Implementation of cPCI-9116 DAQ card digital input channel group in NDS.
 * @author kstrnisa
 * @date 20.8.2013
 */

#include "ndsChannelGroup.h"
#include "ndsChannel.h"

#include "pci9116Device.h"
#include "pci9116DIChannelGroup.h"
#include "pci9116DIChannel.h"

#include <dask.h>
#include "pcidask_errors.h"


/**
 * @brief DIChannelGroup constructor.
 * @param [in] name Channel Group name.
 */
pci9116DIChannelGroup::pci9116DIChannelGroup(const std::string& name) : nds::ChannelGroup(name) {

    registerOnEnterStateHandler(nds::CHANNEL_STATE_ERROR,
            boost::bind(&pci9116DIChannelGroup::onEnterError, this, _1, _2));
};


/**
 * @brief Read DI port.
 * @param [in] pasynUser Asyn user struct.
 * @param [out] value DI channel value.
 * @param [in] mask Mask that determines which channels are read.
 */
ndsStatus pci9116DIChannelGroup::getValueUInt32Digital(asynUser* pasynUser, epicsUInt32 *value, epicsUInt32 mask) {
    U32 chan_data32;
    int status;

    NDS_TRC("%s", __func__);

    status = DI_ReadPort(_CardNumberReg, 0, &chan_data32);
    if (status != NoError ) {
        NDS_ERR("DI_ReadPort error = %s.", pcidask_decode_error(status));
        return ndsError;
    }
    NDS_DBG("DI_ReadPort returned: %u.", (unsigned)chan_data32);

    *value = (epicsUInt32)(chan_data32 & mask);
    return ndsSuccess;
}


/**
 * @brief State handler for transition to ERROR.
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Successful transition.
 *
 * Put the device to ERROR state.
 */
ndsStatus pci9116DIChannelGroup::onEnterError(nds::ChannelStates from, nds::ChannelStates to) {
    NDS_TRC("%s", __func__);

    _device->error();

    return ndsSuccess;
}


/**
 * @brief Set device context for the channel group.
 * @param [in] newCardNumberReg Device context.
 *
 * @retval ndsSuccess Device context set successfully.
 *
 * Remember the device context and push the device context to all the channels.
 */
ndsStatus pci9116DIChannelGroup::setCardNumber(epicsUInt16 newCardNumberReg) {
    _CardNumberReg = newCardNumberReg;
    for(ChannelContainer::iterator iter = _nodes.begin(); iter != _nodes.end(); ++iter) {
        (dynamic_cast<pci9116DIChannel *>(iter->second))->setCardNumber(newCardNumberReg);
    }
    return ndsSuccess;
}

