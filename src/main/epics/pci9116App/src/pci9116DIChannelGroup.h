/**
 * @file pci9116DIChannelGroup.h
 * @brief Header file defining the cPCI-9116 DAQ card digital input channel group class.
 * @author kstrnisa
 * @date 20.8.2013
 */
#ifndef _pci9116DIChannelGroup_h
#define _pci9116DIChannelGroup_h

#include "ndsChannel.h"
#include "ndsChannelGroup.h"


/**
 * @brief cPCI-9116 specific nds::ChannelGroup class that support common DI functionality.
 */
class pci9116DIChannelGroup : public nds::ChannelGroup {
private:
    epicsUInt16 _CardNumberReg;     /**< Device context for userspace library. */

public:
    pci9116DIChannelGroup(const std::string& name);

    virtual ndsStatus getValueUInt32Digital(asynUser *pasynUser, epicsUInt32 *value, epicsUInt32 mask);

    virtual ndsStatus setCardNumber(epicsUInt16 newCardNumberReg);

private:
    ndsStatus onEnterError(nds::ChannelStates from, nds::ChannelStates to);
};

#endif
