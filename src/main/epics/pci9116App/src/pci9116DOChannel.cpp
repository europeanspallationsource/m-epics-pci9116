/**
 * Adlink PCI9116 EPICS module
 * Copyright (C) 2015 Cosylab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @file pci9116DOChannel.cpp
 * @brief Implementation of cPCI-9116 DAQ card digital output channel in NDS.
 * @author jsokolic
 * @date 20.8.2013
 */

#include "pci9116DOChannel.h"
#include "pci9116DOChannelGroup.h"
#include "pci9116Device.h"

#include "ndsADIOChannel.h"

#include <dask.h>
#include "pcidask_errors.h"

/**
 * @brief DO channel constructor.
 */
pci9116DOChannel::pci9116DOChannel() {

    registerOnEnterStateHandler(nds::CHANNEL_STATE_ERROR,
            boost::bind(&pci9116DOChannel::onEnterError, this, _1, _2));
}

/**
 * @brief Set DO channel value.
 * @param [in] pasynUser Asyn user struct.
 * @param [in] value DO channel value.
 */
ndsStatus pci9116DOChannel::setValueInt32(asynUser* pasynUser, epicsInt32 value) {
    int                 status;
    U16                 chan_data;
    nds::ChannelGroup   *cg;
    epicsUInt32         ValueReadback;

    chan_data = value != 0 ? 1 : 0;
    NDS_DBG("Calling DO_WriteLine: %u %u %u %u.", _CardNumberReg, 0, (U16)getChannelNumber(), chan_data);
    status = DO_WriteLine(_CardNumberReg, 0, getChannelNumber(), chan_data);
    if (status != NoError ) {
        NDS_ERR("DO_WriteLine error = %s.", pcidask_decode_error(status));
        error();
        return ndsError;
    }

    getValueInt32(pasynUser, &value);

    cg = getChannelGroup();
    cg->lock();
    (dynamic_cast<pci9116DOChannelGroup *>(cg))->getValueUInt32Digital(pasynUser, &ValueReadback, 0xFF);
    cg->unlock();

    return ndsSuccess;
}

/**
 * @brief Get DO channel value.
 * @param [in] pasynUser Asyn user struct.
 * @param [out] value DO channel value.
 */
ndsStatus pci9116DOChannel::getValueInt32(asynUser* pasynUser, epicsInt32 *value){
    U16 chan_data;
    int status;

    status = DO_ReadLine(_CardNumberReg, 0, (U16)getChannelNumber(), &chan_data);
    if (status != NoError) {
        NDS_ERR("DO_ReadLine error = %s.", pcidask_decode_error(status));
        error();
        return ndsError;
    }
    NDS_DBG("DO_ReadLine returned: %u.", chan_data);

    *value = (epicsInt32)chan_data;
    doCallbacksInt32(*value, _interruptIdValueInt32);
    return ndsSuccess;
}


/**
 * @brief State handler for transition to ERROR.
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Successful transition.
 *
 * Put the AI channel group to ERROR state. If an error happens during IOC init when
 * output records are being updated (AsynDriver mechabism) do nothing since the
 * device will be put into ERROR when it tries to go to ON. Putting it to
 * ERROR here does not play nicely with the NDS state machines.
 */
ndsStatus pci9116DOChannel::onEnterError(nds::ChannelStates from, nds::ChannelStates to) {
    pci9116DOChannelGroup   *cg;

    NDS_TRC("%s", __func__);
    
    if (from == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsSuccess;
    }

    cg = dynamic_cast<pci9116DOChannelGroup *>(getChannelGroup());

    cg->lock();
    cg->error();
    cg->unlock();

    return ndsSuccess;
}


/**
 * @brief Set device context for the channel.
 * @param [in] newCardNumberReg Device context.
 *
 * @retval ndsSuccess Device context set successfully.
 *
 * Remember the device context.
 */
ndsStatus pci9116DOChannel::setCardNumber(epicsUInt16 newCardNumberReg) {
    _CardNumberReg = newCardNumberReg;
    return ndsSuccess;
}

