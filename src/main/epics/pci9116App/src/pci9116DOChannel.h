/**
 * @file pci9116DOChannel.h
 * @brief Header file defining the cPCI-9116 DAQ card digital output channel class.
 * @author kstrnisa
 * @date 20.8.2013
 */
#ifndef _pci9116DOChannel_h
#define _pci9116DOChannel_h

#include "ndsADIOChannel.h"

/**
 * @brief cPCI-9116 specific nds::ADIOChannel class that supports DO channels.
 */
class pci9116DOChannel: public nds::ADIOChannel {
private:
    epicsUInt16 _CardNumberReg;    /**< Device context for userspace library. */

public:
    pci9116DOChannel();

    virtual ndsStatus setValueInt32(asynUser* pasynUser, epicsInt32 value);
    virtual ndsStatus getValueInt32(asynUser* pasynUser, epicsInt32 *value);

    ndsStatus setCardNumber(epicsUInt16 newCardNumberReg);

private:
    ndsStatus onEnterError(nds::ChannelStates from, nds::ChannelStates to);
};

#endif
