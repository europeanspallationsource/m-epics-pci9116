/**
 * Adlink PCI9116 EPICS module
 * Copyright (C) 2015 Cosylab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @file pci9116DOChannelGroup.cpp
 * @brief Implementation of cPCI-9116 DAQ card digital output channel group in NDS.
 * @author kstrnisa
 * @date 20.8.2013
 */

#include "ndsChannelGroup.h"
#include "ndsChannel.h"

#include "pci9116Device.h"
#include "pci9116DOChannelGroup.h"
#include "pci9116DOChannel.h"

#include <dask.h>
#include "pcidask_errors.h"

/**
 * @brief DO ChannelGroup constructor.
 * @param [in] name Channel Group name.
 */
pci9116DOChannelGroup::pci9116DOChannelGroup(const std::string& name) : nds::ChannelGroup(name) {

    registerOnEnterStateHandler(nds::CHANNEL_STATE_DISABLED,
            boost::bind(&pci9116DOChannelGroup::onEnterDisabled, this, _1, _2));

    registerOnEnterStateHandler(nds::CHANNEL_STATE_ERROR,
            boost::bind(&pci9116DOChannelGroup::onEnterError, this, _1, _2));
};


/**
 * @brief Write to DO port.
 * @param [in] pasynUser Asyn user struct.
 * @param [in] value DO channel value.
 * @param [in] mask Mask that determines which channels to write to.
 */
ndsStatus pci9116DOChannelGroup::setValueUInt32Digital(asynUser* pasynUser, epicsUInt32 value, epicsUInt32 mask) {
    int         status;
    epicsInt32  ValueReadback;

    NDS_TRC("%s", __func__);

    NDS_DBG("Calling DO_WritePort: %u %u %u.", _CardNumberReg, 0, value & mask);
    status = DO_WritePort(_CardNumberReg, 0, (U32)(value & mask));
    if (status != NoError ) {
        NDS_ERR("DO_WritePort error = %s.", pcidask_decode_error(status));
        error();
        return ndsError;
    }

    /* Do a readback right away to update readback records. */
    getValueUInt32Digital(pasynUser, &value, mask);

    /* Update output channel record readbacks. */
    for(ChannelContainer::iterator iter = _nodes.begin(); iter != _nodes.end(); ++iter) {
        iter->second->lock();
        (dynamic_cast<pci9116DOChannel *>(iter->second))->getValueInt32(pasynUser, &ValueReadback);
        iter->second->unlock();
    }

    return ndsSuccess;
}

/**
 * @brief Read DO port.
 * @param [in] pasynUser Asyn user struct.
 * @param [out] value DO channel value.
 * @param [in] mask Mask that determines which channels to read.
 */
ndsStatus pci9116DOChannelGroup::getValueUInt32Digital(asynUser* pasynUser, epicsUInt32 *value, epicsUInt32 mask) {
    U32 chan_data32;
    int status;

    NDS_TRC("%s", __func__);

    status = DO_ReadPort(_CardNumberReg, 0, &chan_data32);
    if (status != NoError) {
        NDS_ERR("DO_ReadPort error = %s.", pcidask_decode_error(status));
        error();
        return ndsError;
    }
    NDS_DBG("DO_ReadPort returned: %u.", (unsigned)chan_data32);

    *value = (epicsUInt32)(chan_data32 & mask);
    doCallbacksUInt32Digital(*value, _interruptIdValueUInt32Digital);
    return ndsSuccess;
}


/**
 * @brief State handler for transition to DISABLED.
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Successful transition.
 *
 * After IOC init update the readback records with currently set values.
 */
ndsStatus pci9116DOChannelGroup::onEnterDisabled(nds::ChannelStates from, nds::ChannelStates to) {
    epicsUInt32     ValueReadbackUInt32;
    epicsInt32      ValueReadbackInt32;

    NDS_TRC("%s", __func__);

    /* If we came from IOC_INITIALIZATION update readbacks with values set by PINI records.*/
    if (from == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        getValueUInt32Digital(NULL, &ValueReadbackUInt32, 0xFF);
        for(ChannelContainer::iterator iter = _nodes.begin(); iter != _nodes.end(); ++iter) {
            iter->second->lock();
            (dynamic_cast<pci9116DOChannel *>(iter->second))->getValueInt32(NULL, &ValueReadbackInt32);
            iter->second->unlock();
        }
    }

    return ndsSuccess;
}


/**
 * @brief State handler for transition to ERROR.
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Successful transition.
 *
 * Put the device to ERROR state. If an error happens during IOC init when
 * output records are being updated (AsynDriver mechabism) do nothing since the
 * device will be put into ERROR when it tries to go to ON. Putting it to
 * ERROR here does not play nicely with the NDS state machines.
 */
ndsStatus pci9116DOChannelGroup::onEnterError(nds::ChannelStates from, nds::ChannelStates to) {
    NDS_TRC("%s", __func__);
    
    if (from == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsSuccess;
    }

    _device->error();

    return ndsSuccess;
}


/**
 * @brief Set device context for the channel group.
 * @param [in] newCardNumberReg Device context.
 *
 * @retval ndsSuccess Device context set successfully.
 *
 * Remember the device context and push the device context to all the channels.
 */
ndsStatus pci9116DOChannelGroup::setCardNumber(epicsUInt16 newCardNumberReg) {
    _CardNumberReg = newCardNumberReg;
    for(ChannelContainer::iterator iter = _nodes.begin(); iter != _nodes.end(); ++iter) {
        (dynamic_cast<pci9116DOChannel *>(iter->second))->setCardNumber(newCardNumberReg);
    }
    return ndsSuccess;
}
