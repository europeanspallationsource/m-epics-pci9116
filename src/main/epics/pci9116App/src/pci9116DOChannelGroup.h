/**
 * @file pci9116DOChannelGroup.h
 * @brief Header file defining the cPCI-9116 DAQ card digital output channel group class.
 * @author kstrnisa
 * @date 20.8.2013
 */
#ifndef _pci9116DOChannelGroup_h
#define _pci9116DOChannelGroup_h

#include "ndsChannelGroup.h"

/**
 * @brief cPCI-9116 specific nds::ChannelGroup class that support common DO functionality.
 */
class pci9116DOChannelGroup : public nds::ChannelGroup {
private:
    epicsUInt16 _CardNumberReg;    /**< Device context for userspace library. */

public:
    pci9116DOChannelGroup(const std::string& name);

    virtual ndsStatus setValueUInt32Digital(asynUser* pasynUser, epicsUInt32 value, epicsUInt32 mask);
    virtual ndsStatus getValueUInt32Digital(asynUser* pasynUser, epicsUInt32 *value, epicsUInt32 mask);

    ndsStatus setCardNumber(epicsUInt16 newCardNumberReg);

private:
    ndsStatus onEnterDisabled(nds::ChannelStates from, nds::ChannelStates to);
    ndsStatus onEnterError(nds::ChannelStates from, nds::ChannelStates to);
};

#endif
