/**
 * Adlink PCI9116 EPICS module
 * Copyright (C) 2015 Cosylab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @file pci9116Device.cpp
 * @brief Implementation of cPCI-9116 DAQ card Device in NDS.
 * @author kstrnisa
 * @date 20.8.2013
 */

#include "pci9116Device.h"
#include "pci9116AIChannelGroup.h"
#include "pci9116DIChannelGroup.h"
#include "pci9116DOChannelGroup.h"
#include "pci9116GCTRChannelGroup.h"
#include "pci9116AIChannel.h"
#include "pci9116DIChannel.h"
#include "pci9116DOChannel.h"

#include "ndsManager.h"
#include "ndsChannelGroup.h"
#include "ndsChannel.h"
#include "ndsDevice.h"

#include <dask.h>
#include "pcidask_errors.h"


nds::RegisterDriver<pci9116Device> exportedDevice("pci9116");

/**
 * @brief Device constructor.
 * @param [in] name Device name.
 *
 * Register state transition handlers and message handlers. For details
 * refer to NDS documentation.
 */
pci9116Device::pci9116Device(const std::string& name) : nds::Device(name) {

    registerOnRequestStateHandler(nds::DEVICE_STATE_INIT, nds::DEVICE_STATE_ON,
            boost::bind(&pci9116Device::onSwitchOn, this, _1, _2));

    registerOnEnterStateHandler(nds::DEVICE_STATE_ON,
            boost::bind(&pci9116Device::onEnterOn, this, _1, _2));

    registerOnEnterStateHandler(nds::DEVICE_STATE_OFF,
            boost::bind(&pci9116Device::onEnterOffState, this, _1, _2));

    enableFastInit();
}


/**
 * @brief Create driver structures.
 * @param [in] portName AsynPort name.
 * @param [in] params Refer to NDS documentation.
 *
 * @retval ndsSuccess createStructure always returns success.
 *
 * AI, DI, DO, GCTR and external trigger channel groups are registered.
 * AI, DI and DO channels are registered to corresponding channel groups.
 */
ndsStatus pci9116Device::createStructure(const char* portName, const char* params) {
    int                     iter;
    ndsStatus               status;
    nds::ChannelGroup       *cg;
    pci9116AIChannelGroup   *cgAI;
    pci9116DIChannelGroup   *cgDI;
    pci9116DOChannelGroup   *cgDO;
    pci9116GCTRChannelGroup *cgGCTR;

    NDS_TRC("%s", __func__);

    /* Analog channel group registration. */
    cgAI = new pci9116AIChannelGroup(PCI9116NDS_AICG_NAME);
    status = registerChannelGroup(cgAI);
    if (status != ndsSuccess) {
        NDS_ERR("registerChannelGroup %s error.", PCI9116NDS_AICG_NAME);
        return ndsSuccess;
    }
    for(iter = 0; iter < PCI9116NDS_NUM_AI_CHANNELS; iter++) {
        status = cgAI->registerChannel(new pci9116AIChannel());
        if (status != ndsSuccess) {
            NDS_ERR("registerChannel error for %s channel %d.", PCI9116NDS_AICG_NAME, iter);
            return ndsSuccess;
        }
    }

    /* Digital input channel group registration. */
    cgDI = new pci9116DIChannelGroup(PCI9116NDS_DICG_NAME);
    status = registerChannelGroup(cgDI);
    if (status != ndsSuccess) {
        NDS_ERR("registerChannelGroup %s error.", PCI9116NDS_DICG_NAME);
        return ndsSuccess;
    }
    for(iter = 0; iter < PCI9116NDS_NUM_DIO_CHANNELS; iter++) {
        status = cgDI->registerChannel(new pci9116DIChannel());
        if (status != ndsSuccess) {
            NDS_ERR("registerChannel error for %s channel %d.", PCI9116NDS_DICG_NAME, iter);
            return ndsSuccess;
        }
    }

    /* Digital output channel group registration. */
    cgDO = new pci9116DOChannelGroup(PCI9116NDS_DOCG_NAME);
    status = registerChannelGroup(cgDO);
    if (status != ndsSuccess) {
        NDS_ERR("registerChannelGroup %s error.", PCI9116NDS_DOCG_NAME);
        return ndsSuccess;
    }
    for(iter = 0; iter < PCI9116NDS_NUM_DIO_CHANNELS; iter++) {
        status = cgDO->registerChannel(new pci9116DOChannel());
        if (status != ndsSuccess) {
            NDS_ERR("registerChannel error for %s channel %d.", PCI9116NDS_DOCG_NAME, iter);
            return ndsSuccess;
        }
    }

    /* General purpose timer/counter channel group registration. */
    cgGCTR = new pci9116GCTRChannelGroup(PCI9116NDS_GCTRCG_NAME);
    status = registerChannelGroup(cgGCTR);
    if (status != ndsSuccess) {
        NDS_ERR("registerChannelGroup %s error.", PCI9116NDS_GCTRCG_NAME);
        return ndsSuccess;
    }

    /* External trigger line channel group registration. */
    cg = new nds::ChannelGroup(PCI9116NDS_EXTCG_NAME);
    status = registerChannelGroup(cg);
    if (status != ndsSuccess) {
        NDS_ERR("registerChannelGroup %s error.", PCI9116NDS_EXTCG_NAME);
        return ndsSuccess;
    }
    status = cg->registerChannel(new nds::Channel());
    if (status != ndsSuccess) {
        NDS_ERR("registerChannel error for %s channel %d.", PCI9116NDS_EXTCG_NAME, 0);
        return ndsSuccess;
    }

    _CardNumber = getIntParam("CardNumber", 0);

    /* try registering the card here already so that output records can get values. */
    _InitStatus = registerCard();

    return ndsSuccess;
}


/**
 * @brief State handler for transition to ON.
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Successful transition.
 * @retval ndsError Cannot register card.
 *
 * If the card hasn't been registered yet (before IOC init) then register it.
 */
ndsStatus pci9116Device::onSwitchOn(nds::DeviceStates from, nds::DeviceStates to) {
    NDS_TRC("%s", __func__);

    _InitStatus = registerCard();
    if (_InitStatus != ndsSuccess) {
        NDS_ERR("Cannot connect to card.");
        return ndsError;
    }
    NDS_DBG("Card registered with number %u.", _CardNumberReg);

    return ndsSuccess;
}


/**
 * @brief State handler for transition to ON.
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Successful transition.
 *
 * Push the device context to all the channel groups.
 */
ndsStatus pci9116Device::onEnterOn(nds::DeviceStates from, nds::DeviceStates to) {
    nds::ChannelGroup   *cg;

    NDS_TRC("%s", __func__);

    getChannelGroup(PCI9116NDS_AICG_NAME, &cg);
    (dynamic_cast<pci9116AIChannelGroup *>(cg))->setCardNumber(_CardNumberReg);

    getChannelGroup(PCI9116NDS_DICG_NAME, &cg);
    (dynamic_cast<pci9116DIChannelGroup *>(cg))->setCardNumber(_CardNumberReg);

    getChannelGroup(PCI9116NDS_DOCG_NAME, &cg);
    (dynamic_cast<pci9116DOChannelGroup *>(cg))->setCardNumber(_CardNumberReg);

    getChannelGroup(PCI9116NDS_GCTRCG_NAME, &cg);
    (dynamic_cast<pci9116GCTRChannelGroup *>(cg))->setCardNumber(_CardNumberReg);

    return ndsSuccess;
}


/**
 * @brief State handler for transition to OFF.
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Successful transition.
 *
 * If the card is registered then release it from the userspace library.
 * After IOC init update the readback records with currently set values.
 */
ndsStatus pci9116Device::onEnterOffState(nds::DeviceStates from, nds::DeviceStates to) {
    int status;

    NDS_TRC("%s", __func__);

    if (from == nds::DEVICE_STATE_IOC_INIT) {
        doCallbacksInt32(_isEnabled, _interruptIdEnabled);
    }
    
    if (_InitStatus != ndsSuccess) {
        NDS_DBG("Not releasing card since none has been registered.");
        return ndsSuccess;
    }
    
    status = Release_Card(_CardNumberReg);
    if (status != NoError) {
        NDS_ERR("Release_Card error = %s.", pcidask_decode_error(status));
    }
    NDS_DBG("Card with number %u released.", _CardNumberReg);

    return ndsSuccess;
}


/**
 * @brief Register card with the userspace library.
 *
 * @retval ndsSuccess Card registered successfully.
 * @retval ndsError Failed to register card or read amount of memory available
 * for DMA transfers.
 */
ndsStatus pci9116Device::registerCard() {
    int status;
    U32 memsize;

    NDS_TRC("%s", __func__);

    NDS_DBG("Calling Register_Card with %u %u.", PCI_9116, _CardNumber);
    status = Register_Card(PCI_9116, _CardNumber);
    if (status < 0) {
        NDS_ERR("Register_Card status = %d error = %s.", status, pcidask_decode_error(status));
        return ndsError;
    }
    /* In case of no error this is the device context. */
    _CardNumberReg = (epicsInt16)status;

    /* Checking how much memory is allocated by the kernel module. */
    status = AI_InitialMemoryAllocated(_CardNumberReg, &memsize);
    if (status != NoError) {
        NDS_ERR("AI_InitialMemoryAllocated error = %s.", pcidask_decode_error(status));
        return ndsError;
    }
    NDS_DBG("AI_InitialMemoryAllocated returned %u.", (unsigned)memsize);
    _MemorySize = memsize * 1024; /* MemSize is in KB, but we will need bytes. */

    return ndsSuccess;
}


/**
 * @brief Get the amount of memory available for DMA transfers.
 * @param [in] newCardNumberReg Device context.
 *
 * @retval Amount of memory in bytes.
 */
int pci9116Device::getMemorySize() {
    return _MemorySize;
}

