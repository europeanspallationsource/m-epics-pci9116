/**
 * @file pci9116Device.h
 * @brief Header file defining the cPCI-9116 DAQ card Device class.
 * @author kstrnisa
 * @date 20.8.2013
 */
#ifndef _pci9116Device_h
#define _pci9116Device_h

#include "ndsDevice.h"

#define PCI9116NDS_AICG_NAME        "ai"            /**< ChanelGroup name for analog input channels. */
#define PCI9116NDS_DICG_NAME        "di"            /**< ChanelGroup name for digital input channels. */
#define PCI9116NDS_DOCG_NAME        "do"            /**< ChanelGroup name for digital output channels. */
#define PCI9116NDS_GCTRCG_NAME      "gctr"          /**< ChanelGroup name for GCTR. */
#define PCI9116NDS_EXTCG_NAME       "external"      /**< ChanelGroup name for the external trigger line. */

#define PCI9116NDS_NUM_AI_CHANNELS  64              /**< Number of analog input channels. */
#define PCI9116NDS_NUM_DIO_CHANNELS 8               /**< Number of digital input and output channels. */

#define PCI9116NDS_INT_CLK_FREQ     24000000        /**< Internal clock frequency in Hz. */
#define PCI9116NDS_COUNTER_MIN      96              /**< Minimum value for sample and scan counter. */
#define PCI9116NDS_COUNTER_MAX      0xFFFFFF        /**< Maximum value for scan counter. */
#define PCI9116NDS_GAIN_QUEUE_SIZE  512             /**< Maximum number of elements in gain queue. */

#define PCI9116NDS_DAQ_SLEEP_FACTOR 100             /**< Factor between buffer size and polling period. */
#define PCI9116NDS_DAQ_SLEEP_TIME   0.000004        /**< Minimum polling period. */

/** Macro that locks the object, puts it into ERROR state, unlocks and returns ndsError.
 * Used in DaqTask when an error is encountered and the AI channel group has to
 * be put into ERROR state. */
#define PCI9116NDS_LOCKED_ERROR                     \
        lock();                                     \
        _DaqTask->setState(nds::ndsThreadStopped);  \
        error();                                    \
        unlock();                                   \
        return ndsError


/**
 * @brief Enumeration of DAQ operating modes.
 */
enum pci9116_mode {
    pci9116_mode_finite,                        /**< Acquire only certain number of samples. */
    pci9116_mode_continuous                     /**< Perform continuous data acquisition. */
};

/**
 * @brief Enumeration of clock sources for analog data acquisition.
 */
enum pci9116_timebase {
    pci9116_timebase_internal,                  /**< Internal time base (24Mhz). */
    pci9116_timebase_external                   /**< External time base. */
};

/**
 * @brief Enumeration of possible AD polarities.
 */
enum pci9116_polarity {
    pci9116_polarity_bipolar,                   /**< Bipolar AD mode. */
    pci9116_polarity_unipolar                   /**< Unipolar AD mode. */
};

/**
 * @brief Enumeration of input modes.
 */
enum pci9116_input_mode {
    pci9116_input_mode_single,                  /**< Each channel is connected to ground. */
    pci9116_input_mode_differential             /**< Channels are used in pairs to read differential voltages. */
};

/**
 * @brief Enumeration of grounding options.
 */
enum pci9116_ground {
    pci9116_ground_local,                       /**< Local ground is used. */
    pci9116_ground_user                         /**< User defined ground is used. */
};

/**
 * @brief Enumeration of analog input channels AD ranges.
 */
enum pci9116_range {
    pci9116_range_10V,                          /**< Absolute range 10V, bipolar [-5V,5V], unipolar [0,10V] */
    pci9116_range_5V,                           /**< Absolute range 5V, bipolar [-2.5V,2.5V], unipolar [0,5V] */
    pci9116_range_2V5,                          /**< Absolute range 2.5V, bipolar [-1.25V,1.25V], unipolar [0,2.5V] */
    pci9116_range_1V25,                         /**< Absolute range 1.25V, bipolar [-0.625V,0.625V], unipolar [0,1.25V] */
};

/**
 * @brief Enumeration of GCTR operating modes.
 */
enum pci9116_gtcr_mode {
    pci9116_gtcr_mode_counter,                  /**< GCTR operates as counter. */
    pci9116_gtcr_mode_pulse                     /**< GCTR operates as pulse generator. */
};

/**
 * @brief Enumeration of GCTR clock sources.
 */
enum pci9116_gtcr_clock_source {
    pci9116_gtcr_clock_source_internal,         /**< Internal clock source. */
    pci9116_gtcr_clock_source_external          /**< External clock source. */
};

/**
 * @brief Enumeration of GCTR gate sources.
 */
enum pci9116_gtcr_gate_source {
    pci9116_gtcr_gate_source_internal,          /**< Internal gate source. */
    pci9116_gtcr_gate_source_external           /**< External gate source. */
};

/**
 * @brief Enumeration of GCTR up/down select.
 */
enum pci9116_gtcr_updown_select {
    pci9116_gtcr_updown_soft,                   /**< Software up/down select */
    pci9116_gtcr_updown_external                /**< External up/down select */
};

/**
 * @brief Enumeration of GCTR up/down control.
 */
enum pci9116_gtcr_updown_control {
    pci9116_gtcr_updown_down,                   /**< Count down. */
    pci9116_gtcr_updown_up                      /**< Count up. */
};


/**
 * @brief cPCI-9116 specific nds::Device class.
 */
class pci9116Device : public nds::Device
{
private:
    epicsInt32  _CardNumber;    /**< Driver init parameter to determine which card to use. */
    epicsUInt16 _CardNumberReg; /**< Device context for userspace library. */
    int         _MemorySize;    /**< Memory available for data acquisition. */
    ndsStatus   _InitStatus;    /**< Flag indicating that card was successfuly registered. */

    ndsStatus registerCard();

public:
    pci9116Device(const std::string& name);
    virtual ndsStatus createStructure(const char* portName, const char* params);
    
    int getMemorySize();

private:
    ndsStatus onSwitchOn(nds::DeviceStates from, nds::DeviceStates to);
    ndsStatus onEnterOn(nds::DeviceStates from, nds::DeviceStates to);
    ndsStatus onEnterOffState(nds::DeviceStates from, nds::DeviceStates to);
};


#endif
