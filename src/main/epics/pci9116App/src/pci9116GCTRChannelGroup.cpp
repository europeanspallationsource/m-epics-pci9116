/**
 * Adlink PCI9116 EPICS module
 * Copyright (C) 2015 Cosylab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @file pci9116GCTRChannelGroup.cpp
 * @brief Implementation of cCI-9116 DAQ card general purpose timer/counter channel group in NDS.
 * @author kstrnisa
 * @date 20.8.2013
 */

#include "pci9116Device.h"
#include "pci9116GCTRChannelGroup.h"

#include "ndsChannelStates.h"
#include "ndsChannelStateMachine.h"
#include "ndsChannelGroup.h"
#include "ndsADIOChannel.h"
#include "ndsChannel.h"

#include <dask.h>
#include "pcidask_errors.h"


const epicsUInt16 pci9116GCTRChannelGroup::daskGCTRMode[2] = {General_Counter, Pulse_Generation};
const epicsUInt16 pci9116GCTRChannelGroup::daskGCTRClockSource[2] = {GPTC_CLKSRC_INT, GPTC_CLKSRC_EXT};
const epicsUInt16 pci9116GCTRChannelGroup::daskGCTRGateSource[2] = {GPTC_GATESRC_INT, GPTC_GATESRC_EXT};
const epicsUInt16 pci9116GCTRChannelGroup::daskGCTRUpDownSource[2] = {GPTC_UPDOWN_SELECT_SOFT, GPTC_UPDOWN_SELECT_EXT};
const epicsUInt16 pci9116GCTRChannelGroup::daskGCTRUpDownControl[2] = {GPTC_DOWN_CTR, GPTC_UP_CTR};
const epicsUInt16 pci9116GCTRChannelGroup::daskGCTRState[2] = {GPTC_ENABLE, GPTC_DISABLE};

/**
 * @brief GCTR ChannelGroup constructor.
 * @param [in] name Channel Group name.
 *
 * Register state transition handlers and message handlers. For details
 * refer to NDS documentation.
 */
pci9116GCTRChannelGroup::pci9116GCTRChannelGroup(const std::string& name) : nds::ChannelGroup(name) {

    registerOnRequestStateHandler(nds::CHANNEL_STATE_DISABLED, nds::CHANNEL_STATE_PROCESSING,
            boost::bind(&pci9116GCTRChannelGroup::onSwitchProcessing, this, _1, _2));

    registerOnLeaveStateHandler(nds::CHANNEL_STATE_PROCESSING,
            boost::bind(&pci9116GCTRChannelGroup::onLeaveProcessing, this, _1, _2 ));

    registerOnEnterStateHandler(nds::CHANNEL_STATE_DISABLED,
            boost::bind(&pci9116GCTRChannelGroup::onEnterDisabled, this, _1, _2));

    registerOnEnterStateHandler(nds::CHANNEL_STATE_ERROR,
            boost::bind(&pci9116GCTRChannelGroup::onEnterError, this, _1, _2));
};

/**
 * @brief Registers PV handlers.
 * @param [in] pvContainers Refer to NDS documentation.
 *
 * For details refer to NDS documentation.
 */
ndsStatus pci9116GCTRChannelGroup::registerHandlers(nds::PVContainers* pvContainers) {
    nds::ChannelGroup::registerHandlers(pvContainers);

    NDS_PV_REGISTER_INT32("ReadGCTR", &pci9116GCTRChannelGroup::setInt32,
            &pci9116GCTRChannelGroup::readGCTR, &_interruptIdReadGCTR);
    NDS_PV_REGISTER_INT32("GateSource", &pci9116GCTRChannelGroup::setGateSource,
            &pci9116GCTRChannelGroup::getInt32, &_interruptIdGateSource);
    NDS_PV_REGISTER_INT32("UpDownSource", &pci9116GCTRChannelGroup::setUpDownSource,
            &pci9116GCTRChannelGroup::getInt32, &_interruptIdUpDownSource);
    NDS_PV_REGISTER_INT32("UpDownControl", &pci9116GCTRChannelGroup::setUpDownControl,
            &pci9116GCTRChannelGroup::getInt32, &_interruptIdUpDownControl);
    NDS_PV_REGISTER_INT32("Count", &pci9116GCTRChannelGroup::setCount,
            &pci9116GCTRChannelGroup::getInt32, &_interruptIdCount);

    return ndsSuccess;
}


/**
 * @brief Set GCTR operation mode.
 * @param [in] pasynUser Asyn user struct.
 * @param [in] mode GCTR operating mode to set.
 *
 * @retval ndsSuccess Parameter set successfully.
 * @retval ndsError Parameter invalid.
 */
ndsStatus pci9116GCTRChannelGroup::setProcessingMode(asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    if (value < pci9116_gtcr_mode_counter || value > pci9116_gtcr_mode_pulse) {
        NDS_ERR("Invalid processing mode.");
        return ndsError;
    }

    _ProcessingMode = value;
    doCallbacksInt32(_ProcessingMode, _interruptIdProcessingMode);
    return ndsSuccess;
}


/**
 * @brief Set GCTR clock source.
 * @param [in] pasynUser Asyn user struct.
 * @param [in] value GCTR clock source to set.
 *
 * @retval ndsSuccess Parameter set successfully.
 * @retval ndsError Parameter invalid.
 */
ndsStatus pci9116GCTRChannelGroup::setClockSource(asynUser* pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    if (value < pci9116_gtcr_clock_source_internal || value > pci9116_gtcr_clock_source_external) {
        NDS_ERR("Invalid clock source.");
        return ndsError;
    }

    _ClockSource = value;
    doCallbacksInt32(_ClockSource, _interruptIdClockSource);
    return ndsSuccess;
}


/**
 * @brief Set GCTR gating source.
 * @param [in] pasynUser Asyn user struct.
 * @param [in] value GCTR gating source to set.
 *
 * @retval ndsSuccess Parameter set successfully.
 * @retval ndsError Parameter invalid.
 */
ndsStatus pci9116GCTRChannelGroup::setGateSource(asynUser* pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    if (value < pci9116_gtcr_gate_source_internal || value > pci9116_gtcr_gate_source_external) {
        NDS_ERR("Invalid gate source.");
        return ndsError;
    }

    _GateSource = value;
    doCallbacksInt32(_GateSource, _interruptIdGateSource);
    return ndsSuccess;
}

/**
 * @brief Set GCTR up/down control source.
 * @param [in] pasynUser Asyn user struct.
 * @param [in] value GCTR up/down control source to set.
 *
 * @retval ndsSuccess Parameter set successfully.
 * @retval ndsError Parameter invalid.
 */
ndsStatus pci9116GCTRChannelGroup::setUpDownSource(asynUser* pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    if (value < pci9116_gtcr_updown_soft || value > pci9116_gtcr_updown_external) {
        NDS_ERR("Invalid up/down source.");
        return ndsError;
    }

    _UpDownSource = value;
    doCallbacksInt32(_UpDownSource, _interruptIdUpDownSource);
    return ndsSuccess;
}

/**
 * @brief Set GCTR counting direction.
 * @param [in] pasynUser Asyn user struct.
 * @param [in] value GCTR counting direction to set.
 *
 * @retval ndsSuccess Parameter set successfully.
 * @retval ndsError Parameter invalid.
 */
ndsStatus pci9116GCTRChannelGroup::setUpDownControl(asynUser* pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    if (value < pci9116_gtcr_updown_down || value > pci9116_gtcr_updown_up) {
        NDS_ERR("Invalid up/down control.");
        return ndsError;
    }

    _UpDownControl = value;
    doCallbacksInt32(_UpDownControl, _interruptIdUpDownControl);
    return ndsSuccess;
}

/**
 * @brief Set GCTR counter starting value.
 * @param [in] pasynUser Asyn user struct.
 * @param [in] value GCTR counter starting value to set.
 *
 * @retval ndsSuccess Parameter set successfully.
 * @retval ndsError Parameter invalid.
 */
ndsStatus pci9116GCTRChannelGroup::setCount(asynUser* pasynUser, epicsInt32 value) {
    ndsStatus status;

    NDS_TRC("%s", __func__);

    status = ndsSuccess;

    if (value < 0) {
        value = 0;
        status = ndsError;
    }
    if (value > USHRT_MAX) {
        value = USHRT_MAX;
        status = ndsError;
    }

    if (status != ndsSuccess) {
        NDS_ERR("Counter value out of bounds.");
    }

    _Count = value;
    doCallbacksInt32(_Count, _interruptIdCount);
    return status;
}


/**
 * @brief Get GCTR counter value.
 * @param [in] pasynUser Asyn user struct.
 * @param [out] value Current GCTR value.
 *
 * @retval ndsSuccess Value read successfuly.
 * @retval ndsError Failed to read value.
 */
ndsStatus pci9116GCTRChannelGroup::readGCTR(asynUser* pasynUser, epicsInt32 *value) {
    int status;
    U32 chan_data32;

    NDS_TRC("%s", __func__);

    status = GCTR_Read(_CardNumberReg, 0, &chan_data32);
    if (status != NoError) {
        NDS_ERR("GCTR_Read error = %s.", pcidask_decode_error(status));
        error();
        return ndsError;
    }
    NDS_DBG("GCTR_Read returned with: %u.", (unsigned)chan_data32);

    *value = (epicsInt32)chan_data32;
    return ndsSuccess;
}


/**
 * @brief State handler for transition from ON to PROCESSING.
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Successful transition.
 * @retval ndsError Failed to configure the GCTR.
 *
 * Construct the compound GCTR control parameter and configure the GCTR.
 */
ndsStatus  pci9116GCTRChannelGroup::onSwitchProcessing(nds::ChannelStates, nds::ChannelStates) {
    int status;

    NDS_TRC("%s", __func__);

    _GCTRControl = 0;
    _GCTRControl |= daskGCTRMode[_ProcessingMode];
    _GCTRControl |= daskGCTRClockSource[_ClockSource];
    _GCTRControl |= daskGCTRGateSource[_GateSource];
    _GCTRControl |= daskGCTRUpDownSource[_UpDownSource];
    _GCTRControl |= daskGCTRUpDownControl[_UpDownControl];
    _GCTRControl |= daskGCTRState[0];

    NDS_DBG("Calling GCTR_Setup: %u %u 0x%x %u.", _CardNumberReg, 0, _GCTRControl, _Count);
    status = GCTR_Setup(_CardNumberReg, 0, _GCTRControl, (U32)_Count);
    if (status != NoError) {
        NDS_ERR("GCTR_Setup error = %s.", pcidask_decode_error(status));
        return ndsError;
    }

    return ndsSuccess;
}


/**
 * @brief State handler for transition from PROCESSING.
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Successful transition.
 * @retval ndsError Failed to stop the GCTR.
 *
 * Stop counter/timer operation.
 */
ndsStatus  pci9116GCTRChannelGroup::onLeaveProcessing(nds::ChannelStates, nds::ChannelStates) {
    int status;

    NDS_TRC("%s", __func__);

    NDS_DBG("Calling GCTR_Clear: %u %u.", _CardNumberReg, 0);
    status = GCTR_Clear(_CardNumberReg, 0);
    if (status != NoError) {
        NDS_ERR("GCTR_Clear error = %s.", pcidask_decode_error(status));
        return ndsError;
    }

    return ndsSuccess;
}


/**
 * @brief State handler for transition to DISABLED.
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Successful transition.
 *
 * After IOC init update the readback records with currently set values.
 */
ndsStatus pci9116GCTRChannelGroup::onEnterDisabled(nds::ChannelStates from, nds::ChannelStates to) {
    NDS_TRC("%s", __func__);

    /* If we came from IOC_INITIALIZATION update readbacks with values set by PINI records.*/
    if (from == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        doCallbacksInt32(_ProcessingMode, _interruptIdProcessingMode);
        doCallbacksInt32(_ClockSource, _interruptIdClockSource);
        doCallbacksInt32(_GateSource, _interruptIdGateSource);
        doCallbacksInt32(_UpDownControl, _interruptIdUpDownControl);
        doCallbacksInt32(_UpDownSource, _interruptIdUpDownSource);
        doCallbacksInt32(_Count, _interruptIdCount);
    }

    return ndsSuccess;
}


/**
 * @brief State handler for transition to ERROR.
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Successful transition.
 *
 * Put the device to ERROR state.
 */
ndsStatus pci9116GCTRChannelGroup::onEnterError(nds::ChannelStates from, nds::ChannelStates to) {
    NDS_TRC("%s", __func__);

    _device->error();

    return ndsSuccess;
}


/**
 * @brief Set device context for the channel group.
 * @param [in] newCardNumberReg Device context.
 *
 * @retval ndsSuccess Device context set successfully.
 *
 * Remember the device context.
 */
ndsStatus pci9116GCTRChannelGroup::setCardNumber(epicsUInt16 newCardNumberReg) {
    _CardNumberReg = newCardNumberReg;
    return ndsSuccess;
}
