/**
 * @file pci9116GCTRChannelGroup.h
 * @brief Header file defining the cPCI-9116 DAQ card general purpose timer/counter channel group class.
 * @author kstrnisa
 * @date 20.8.2013
 */
#ifndef _pci9116GCTRChannelGroup_h
#define _pci9116GCTRChannelGroup_h

#include "ndsChannelGroup.h"

#include <dask.h>

/**
 * @brief cPCI-9116 specific nds::ChannelGroup class that support General counter/timer functionality.
 */
class pci9116GCTRChannelGroup : public nds::ChannelGroup {
private:
    epicsUInt16 _CardNumberReg;     /**< Device context for userspace library. */

    int _interruptIdReadGCTR;
    int _interruptIdGateSource;
    int _interruptIdUpDownSource;
    int _interruptIdUpDownControl;
    int _interruptIdCount;

    /* Internal variables for card parameters. */
    epicsInt32 _GateSource;         /**< GCTR gating source (internal, external). */
    epicsInt32 _UpDownSource;       /**< GCTR counting direction source (internal, external). */
    epicsInt32 _UpDownControl;      /**< GCTR counting direction setting (up/down). */
    epicsInt32 _Count;              /**< GCTR counter starting value. */
    
    epicsUInt16 _GCTRControl;       /**< Holds compound settings for GCTR control. */

public:
    pci9116GCTRChannelGroup(const std::string& name);
    virtual ndsStatus registerHandlers(nds::PVContainers* pvContainers);

    virtual ndsStatus setProcessingMode(asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus setClockSource(asynUser* pasynUser, epicsInt32 value);
    ndsStatus setGateSource(asynUser* pasynUser, epicsInt32 value);
    ndsStatus setUpDownSource(asynUser* pasynUser, epicsInt32 value);
    ndsStatus setUpDownControl(asynUser* pasynUser, epicsInt32 value);
    ndsStatus setCount(asynUser* pasynUser, epicsInt32 value);
    ndsStatus readGCTR(asynUser* pasynUser, epicsInt32 *value);
    
    ndsStatus setCardNumber(epicsUInt16 newCardNumberReg);

    static const epicsUInt16 daskGCTRMode[2];           /**< Translation between NDS and pcidask macros for GCTR mode. */
    static const epicsUInt16 daskGCTRClockSource[2];    /**< Translation between NDS and pcidask macros for GCTR clock source. */
    static const epicsUInt16 daskGCTRGateSource[2];     /**< Translation between NDS and pcidask macros for GCTR gating source. */
    static const epicsUInt16 daskGCTRUpDownSource[2];   /**< Translation between NDS and pcidask macros for GCTR counting direction source. */
    static const epicsUInt16 daskGCTRUpDownControl[2];  /**< Translation between NDS and pcidask macros for GCTR counting direction (in software direction control). */
    static const epicsUInt16 daskGCTRState[2];          /**< Translation between NDS and pcidask macros for GCTR enable/disable. */

private:
    ndsStatus onLeaveProcessing(nds::ChannelStates from, nds::ChannelStates to);
    ndsStatus onSwitchProcessing(nds::ChannelStates, nds::ChannelStates);
    ndsStatus onEnterDisabled(nds::ChannelStates from, nds::ChannelStates to);
    ndsStatus onEnterError(nds::ChannelStates from, nds::ChannelStates to);
};

#endif
