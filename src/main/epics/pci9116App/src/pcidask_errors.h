
#ifndef PCIDASK_ERRORS_H
#define PCIDASK_ERRORS_H

static const int PCIDASK_ERR_NUM = 68;

static const char* pcidask_errorStrings[PCIDASK_ERR_NUM] = {
   "NoError",
   "ErrorUnknownCardType",
   "ErrorInvalidCardNumber",
   "ErrorTooManyCardRegistered",
   "ErrorCardNotRegistered",
   "ErrorFuncNotSupport",
   "ErrorInvalidIoChannel",
   "ErrorInvalidAdRange",
   "ErrorContIoNotAllowed",
   "ErrorDiffRangeNotSupport",
   "ErrorLastChannelNotZero",
   "ErrorChannelNotDescending",
   "ErrorChannelNotAscending",
   "ErrorOpenDriverFailed",
   "ErrorOpenEventFailed",
   "ErrorTransferCountTooLarge",
   "ErrorNotDoubleBufferMode",
   "ErrorInvalidSampleRate",
   "ErrorInvalidCounterMode",
   "ErrorInvalidCounter",
   "ErrorInvalidCounterState",
   "ErrorInvalidBinBcdParam",
   "ErrorBadCardType",
   "ErrorInvalidDaRefVoltage",
   "ErrorAdTimeOut",
   "ErrorNoAsyncAI",
   "ErrorNoAsyncAO",
   "ErrorNoAsyncDI",
   "ErrorNoAsyncDO",
   "ErrorNotInputPort",
   "ErrorNotOutputPort",
   "ErrorInvalidDioPort",
   "ErrorInvalidDioLine",
   "ErrorContIoActive",
   "ErrorDblBufModeNotAllowed",
   "ErrorConfigFailed",
   "ErrorInvalidPortDirection",
   "ErrorBeginThreadError",
   "ErrorInvalidPortWidth",
   "ErrorInvalidCtrSource",
   "ErrorOpenFile",
   "ErrorAllocateMemory",
   "ErrorDaVoltageOutOfRange",
   "ErrorLockMemory",
   "ErrorDIODataWidthError",
   "ErrorTaskCodeError",
   "ErrortriggercountError",
   "ErrorInvalidTriggerMode",
   "ErrorInvalidTriggerType",
   "ErrorInvalidCounterValue",
   "ErrorInvalidEventHandle",
   "ErrorNoMessageAvailable",
   "ErrorEventMessgaeNotAdded",
   "ErrorCalibrationTimeOut",
   "ErrorUndefinedParameter",
   "ErrorInvalidBufferID",
   "ErrorInvalidSampledClock",
   "ErrorInvalisOperationMode"
};

// rgajsek: added out of bounds check
#define pcidask_decode_error(err) -err > PCIDASK_ERR_NUM ? \
	"Unknown error" : pcidask_errorStrings[-err]

#endif //PCIDASK_ERRORS_H
