#!/usr/bin/python
# EPICS cPci-9116 DAQ card test script
# For the communication with the database we use the Cachannel module, testing is done with the unittest module
#from CaChannel import CaChannel
from CaChannel import *
import unittest
import os
import sys
import time
import argparse

# We set the epics global variable so PVs are accessible from the script.
# Comment this out if the firewall is disabled (default is on)
#os.putenv('EPICS_CA_ADDR_LIST','127.0.0.1')

parser = argparse.ArgumentParser(description='Tests cPci-9116 module specifications')
parser.add_argument('--deviceName', default='PCI9116')
parser.add_argument('unittest_args', nargs='*')    
args = parser.parse_args()
sys.argv[1:] = args.unittest_args

class Test(unittest.TestCase):
# Default device name
    device = args.deviceName
    pv_dev = CaChannel()
    pv_dev.searchw(device)
    device_state = pv_dev.getw()

  
    def setUp(self):
        print 'Set up'
        # Check if ON state
        if self.device_state != 4:
            print 'Device is not in ON state. Turning ON ...'
            device = args.deviceName
            dev_msgs = CaChannel()
            dev_msgs.searchw(device+'-MSGS')
            dev_msgs.putw('ON')
            time.sleep(1)
            pv_dev = CaChannel()
            pv_dev.searchw(device)
            self.device_state = pv_dev.getw()
            self.assertEqual(self.device_state, 4, 'Device is not in ON state. Test will fail')   
    
    
    def tearDown(self):
        # Reset
        dev_msgs = CaChannel()
        dev_msgs.searchw(args.deviceName + '-MSGS')
        dev_msgs.array_put('OFF\0')
        time.sleep(1)
        dev_msgs = CaChannel()
        dev_msgs.searchw(args.deviceName + '-MSGS')
        dev_msgs.array_put('ON\0')

        print 'clean up'
        
    
    def disableDifferentialCh(self):
        # Disable channels that are forbidden in differential AD mode (> 31)
        ch_enbl = CaChannel()
        for ch_index in range(32, 64):
            ch_enbl.searchw(self.device+'-'+'AI'+str(ch_index)+'-ENBL')
            ch_enbl.putw(0)
            
    def enableDifferentialCh(self):
        # Enable channels that are forbidden in differential AD mode (> 31)
        ch_enbl = CaChannel()
        for ch_index in range(32, 64):
            ch_enbl.searchw(self.device+'-'+'AI'+str(ch_index)+'-ENBL')
            ch_enbl.putw(1)
        
#########################################################################################################        
#########################################################################################################
#########################################################################################################        

    
# Check setting of DO CHs and CG and reading of DI CHs and CG.
    def test_TC_01(self):
        print 'TC-01 Testing DI and DO'
        temp = raw_input('Connect all DI inputs to DO outputs (DI0-DO0, DI1-DO1, ...) and press enter')
        
        pv_docg = CaChannel()        
        pv_docg.searchw(self.device+'-'+'DO')
        pv_docg_rbv = CaChannel()
        pv_docg_rbv.searchw(self.device+'-'+'DO-RBV')
        
        # DI channel group
        pv_dicg = CaChannel()
        pv_dicg.searchw(self.device + '-DI')
        pv_dicg_proc = CaChannel()
        pv_dicg_proc.searchw(self.device+'-DI.PROC')
        
        # Testing DO Channel group command
        for set_val in [0, 255, 0]:
            pv_docg.putw(set_val)
            time.sleep(0.2)
            docg_rbv = pv_docg_rbv.getw()
            self.assertTrue(docg_rbv == set_val, 
                            'DO CG read back is wrong {0} != {1}'.format(docg_rbv, set_val))
        
        
        # Testing individual bits
        for i in range(0,8):
            pv_doch = CaChannel()
            pv_doch.searchw(self.device+'-'+'DO' + str(i) + '-OUT')
            pv_doch_rbv = CaChannel()
            pv_doch_rbv.searchw(self.device+'-'+'DO' + str(i) + '-OUT-RBV')
            pv_dich = CaChannel()
            pv_dich.searchw(self.device+'-'+'DI' + str(i) + '-IN')
            pv_dich_proc = CaChannel()
            pv_dich_proc.searchw(self.device+'-'+'DI' + str(i) + '-IN.PROC')
            
            # Set to 0
            pv_doch.putw(0)
            time.sleep(0.1)
            pv_dich_proc.putw(1)
            self.assertTrue(pv_doch_rbv.getw() == 0, 'Clearing DO channel failed')
            self.assertTrue(pv_dich.getw() == 0, 'Reading DI channel failed')  
            
            # Transition to 1
            pv_doch.putw(1)
            time.sleep(0.1)
            pv_dich_proc.putw(1)
            pv_dicg_proc.putw(1)
            time.sleep(0.1)
            self.assertTrue(pv_doch_rbv.getw() == 1, 'Setting DO channel failed')
            self.assertTrue(pv_dich.getw() == 1, 'Reading DI channel failed')
            self.assertTrue(pv_dicg.getw() == 1 << i, 'Reading DI channel group failed')
            
            
            # Transition to 0
            pv_doch.putw(0)
            time.sleep(0.1)
            pv_dich_proc.putw(1)
            self.assertTrue(pv_doch_rbv.getw() == 0, 'Clearing DO channel failed')
            self.assertTrue(pv_dich.getw() == 0, 'Reading DI channel failed')
            
        # proces AI CG so that it goes back to 0.
        pv_dicg_proc.putw(1)


#########################################################################################################        
#########################################################################################################
#########################################################################################################

            
# Checking simple AI read.
    def test_TC_02(self):
        print 'TC-02 Testing simple AI read'
        print 'Connect U_CMMD, AI0, AI33 to positive voltage source (between +-5 V)'
        print 'Connect AGND, AI1, AI32 to voltage source ground' 
        voltage = float(raw_input('Enter the voltage (in Volts) that you have connected to the channels and press enter\n'))
        delta  = int(raw_input('Enter the acceptable measurement deviation (in number of decimal delta)and press enter\n'))
        
        pv_pol = CaChannel() 
        pv_gnd = CaChannel() 
        pv_diff = CaChannel() 
        pv_pol.searchw(self.device+'-'+'AI-RNG')
        pv_gnd.searchw(self.device+'-'+'AI-GND')
        pv_diff.searchw(self.device+'-'+'AI-DIFF')
        pv_pol_rbv = CaChannel() 
        pv_gnd_rbv = CaChannel() 
        pv_diff_rbv = CaChannel() 
        pv_pol_rbv.searchw(self.device+'-'+'AI-RNG-RBV')
        pv_gnd_rbv.searchw(self.device+'-'+'AI-GND-RBV')
        pv_diff_rbv.searchw(self.device+'-'+'AI-DIFF-RBV')
        
        
        pv_ai0 = CaChannel() 
        pv_ai1 = CaChannel() 
        pv_ai32 = CaChannel() 
        pv_ai33 = CaChannel()
        pv_ai0_range = CaChannel() 
        pv_ai1_range = CaChannel() 
        pv_ai32_range = CaChannel() 
        pv_ai33_range = CaChannel()
        pv_ai0_proc = CaChannel() 
        pv_ai1_proc = CaChannel() 
        pv_ai32_proc = CaChannel() 
        pv_ai33_proc = CaChannel() 
        pv_ai0.searchw(self.device+'-'+'AI0-IN')
        pv_ai1.searchw(self.device+'-'+'AI1-IN')
        pv_ai32.searchw(self.device+'-'+'AI32-IN')
        pv_ai33.searchw(self.device+'-'+'AI33-IN')
        pv_ai0_range.searchw(self.device+'-'+'AI0-RNG')
        pv_ai1_range.searchw(self.device+'-'+'AI1-RNG')
        pv_ai32_range.searchw(self.device+'-'+'AI32-RNG')
        pv_ai33_range.searchw(self.device+'-'+'AI33-RNG')
        pv_ai0_proc.searchw(self.device+'-'+'AI0-IN.PROC')
        pv_ai1_proc.searchw(self.device+'-'+'AI1-IN.PROC')
        pv_ai32_proc.searchw(self.device+'-'+'AI32-IN.PROC')
        pv_ai33_proc.searchw(self.device+'-'+'AI33-IN.PROC') 
        
        channel_range = [pv_ai0_range, pv_ai1_range, pv_ai32_range, pv_ai33_range]
        channel_proc = [pv_ai0_proc, pv_ai1_proc, pv_ai32_proc, pv_ai33_proc]
        channel = [pv_ai0, pv_ai1, pv_ai32, pv_ai33]
        val_local_single = [voltage, 0, 0, voltage]
        val_common_single = [0, -voltage, -voltage, 0]
        val_differential = [voltage, -voltage, 0, 0]
        # setting bipolar AD and range +-5V for all channels
        pv_pol.putw(0)
        time.sleep(0.01)
        self.assertTrue(pv_pol_rbv.getw() == 0, 'Could not set bipolar AD mode')
        for ch in channel_range:
            ch.putw(0)
        
        print 'Ground: common, Differential mode: single ended'
        # ground: pci, single ended
        pv_gnd.putw(0)
        time.sleep(0.01)
        self.assertTrue(pv_gnd_rbv.getw() == 0, 'Could not set local ground')
        pv_diff.putw(0)
        self.assertTrue(pv_diff_rbv.getw() == 0, 'Could not set single ended mode')
        
        for ch_proc in channel_proc:
        	ch_proc.putw(1)
        
        for ch, val in zip(channel, val_local_single):
        	print 'val: ' + str(val) + ' read: ' + str(ch.getw())
        	self.assertAlmostEqual(ch.getw(), val, delta, 'Channel value read incorrect')
        	
        print 'Ground: local, Differential mode: single ended'	    
        # ground: user defined, single ended
        pv_gnd.putw(1)
        time.sleep(0.01)
        self.assertTrue(pv_gnd_rbv.getw() == 1, 'Could not set common ground')
        pv_diff.putw(0)
        self.assertTrue(pv_diff_rbv.getw() == 0, 'Could not set single ended mode')
        
        for ch_proc in channel_proc:
        	ch_proc.putw(1)
        
        for ch, val in zip(channel, val_common_single):
        	print 'val: ' + str(val) + ' read: ' + str(ch.getw())
        	self.assertAlmostEqual(ch.getw(), val, delta, 'Channel value read incorrect')  
        
        print 'Ground: common, Differential mode: differential'
        # ground: pci, differential
        pv_gnd.putw(0)
        time.sleep(0.01)
        self.assertTrue(pv_gnd_rbv.getw() == 0, 'Could not set local ground')
        # Setting differential mode should fail since a channel > 31 is enabled
        pv_diff.putw(1)
        self.assertTrue(pv_diff_rbv.getw() == 0, 'Differential AD mode should be forbidden')
        # Disable the offending channels and try again
        self.disableDifferentialCh()
        pv_diff.putw(1)
        self.assertTrue(pv_diff_rbv.getw() == 1, 'Could not ser differential AD mode')
        
        for ch_proc in channel_proc:
        	ch_proc.putw(1)
        
        i=0	
        for ch, val in zip(channel, val_differential):
            print 'val: ' + str(val) + ' read: ' + str(ch.getw())
            if i < 2:
            	self.assertAlmostEqual(ch.getw(), val, delta, 'Channel value read incorrect')
            i = i+1        
       
        pv_diff.putw(0)
        self.assertTrue(pv_diff_rbv.getw() == 0, 'Could not set single ended mode')
        self.enableDifferentialCh()
        


#########################################################################################################        
#########################################################################################################
#########################################################################################################


# Checking triggering. Software, post, pre, middle
    def test_TC_03(self):
        print 'TC-03 Triggering: software, post-, pre- and middle-trigger'
        print 'Connect U_CMMD, AI0, AI33 to positive voltage source (between +-5 V)'
        print 'Connect AGND, AI1, AI32 to voltage source ground' 
        print 'Connect DO0-OUT to External Digital Trigger Signal' 
        voltage = float(raw_input('Enter the voltage (in Volts) that you have connected to the channels and press enter\n'))
        places  = int(raw_input('Enter the acceptable measurement deviation (in number of decimal places)and press enter\n'))
        
        pv_pol = CaChannel() 
        pv_gnd = CaChannel() 
        pv_diff = CaChannel()
        pv_pm = CaChannel() 
        pv_pol.searchw(self.device+'-'+'AI-RNG')
        pv_gnd.searchw(self.device+'-'+'AI-GND')
        pv_diff.searchw(self.device+'-'+'AI-DIFF')
        pv_pm.searchw(self.device+'-'+'AI-PM')
        
        pv_trgc = CaChannel()
        pv_trgr = CaChannel()
        pv_trgd = CaChannel()
        pv_trg = CaChannel()
        pv_trgc.searchw(self.device+'-'+'AI-TRGC')
        pv_trgr.searchw(self.device+'-'+'AI-TRGR')
        pv_trgd.searchw(self.device+'-'+'AI-TRGD')
        pv_trg.searchw(self.device+'-'+'AI-TRG')
        
        pv_smnm = CaChannel()
        pv_smnm.searchw(self.device+'-'+'AI-SMNM')
        pv_smnm_rbv = CaChannel()
        pv_smnm_rbv.searchw(self.device+'-'+'AI-SMNM-RBV')
        
        pv_clkf = CaChannel()
        pv_clkf.searchw(self.device+'-'+'AI-CLKF')
        
        pv_ai_msgs = CaChannel()
        pv_ai_stat = CaChannel()
        pv_ai_msgs.searchw(self.device+'-AI-MSGS')
        pv_ai_stat.searchw(self.device+'-AI-STAT')
        
        pv_ai0 = CaChannel() 
        pv_ai1 = CaChannel() 
        pv_ai32 = CaChannel() 
        pv_ai33 = CaChannel()
        pv_ai0_range = CaChannel() 
        pv_ai1_range = CaChannel() 
        pv_ai32_range = CaChannel() 
        pv_ai33_range = CaChannel()
        pv_ai0_msgs = CaChannel() 
        pv_ai1_msgs = CaChannel() 
        pv_ai32_msgs = CaChannel() 
        pv_ai33_msgs = CaChannel() 
        pv_ai0.searchw(self.device+'-'+'AI0')
        pv_ai1.searchw(self.device+'-'+'AI1')
        pv_ai32.searchw(self.device+'-'+'AI32')
        pv_ai33.searchw(self.device+'-'+'AI33') 
        pv_ai0_range.searchw(self.device+'-'+'AI0-RNG')
        pv_ai1_range.searchw(self.device+'-'+'AI1-RNG')
        pv_ai32_range.searchw(self.device+'-'+'AI32-RNG')
        pv_ai33_range.searchw(self.device+'-'+'AI33-RNG')
        pv_ai0_msgs.searchw(self.device+'-'+'AI0-MSGS')
        pv_ai1_msgs.searchw(self.device+'-'+'AI1-MSGS')
        pv_ai32_msgs.searchw(self.device+'-'+'AI32-MSGS')
        pv_ai33_msgs.searchw(self.device+'-'+'AI33-MSGS') 
        
        pv_d_trg = CaChannel()
        pv_d_trg.searchw(self.device+'-'+'DO0-OUT')
        
        channel_range = [pv_ai0_range, pv_ai1_range, pv_ai32_range, pv_ai33_range]
        channel_msgs = [pv_ai0_msgs, pv_ai1_msgs, pv_ai32_msgs, pv_ai33_msgs]
        channel = [pv_ai0, pv_ai1, pv_ai32, pv_ai33]
        val0 = range(0,200)
        val1 = range(0,200)
        val32 =range(0,200)
        val33 = range(0,200)
        value = [val0, val1, val32, val33]  
        val_local_single = [voltage, 0, 0, voltage]
        val_common_single = [0, -voltage, -voltage, 0]
        val_differential = [voltage, -voltage, 0, 0]
        
        samples_count = 100
        ad_frequency = 1000

        # setting finite, bipolar, local ground, single ended mode
        pv_pol.putw(0)   
        pv_gnd.putw(0)
        pv_diff.putw(0)
        pv_pm.putw(0)
        # setting range +-5V for all channels
        for ch in channel_range:
            ch.putw(0)
        
        # number of samples
        pv_smnm.putw(samples_count)
        time.sleep(0.1)
        self.assertEqual(pv_smnm_rbv.getw(), samples_count, 
                         'Error setting Scan repeat to {0}'.format(samples_count))
        
        quasiClearWaveform(pv_trg, pv_smnm, pv_ai_stat)
        time.sleep(0.1)
             
        print 'Software trigger'
        
        pv_trg.putw(0) # software trigger

        time.sleep(0.1)
        while pv_ai_stat.getw() == 4: # processing
            time.sleep(0.1)
            #print 'stat: ' + str(pv_ai_stat.getw())

        for ch, ref_v in zip(channel, val_local_single) :
            ch.array_get()
            ch.pend_io()
            v = ch.getValue()
            ind = 0
            for d in v:
                if ind < samples_count:
                    #print 'index: ' + str(ind) + ', d: ' + str(d) 
                    self.assertAlmostEqual(d, ref_v, places,
                                           ('Channel value read incorrect at '+
                                            '[{0}]{1} vs. {2}'.format(ind,d, ref_v)))
                else:
                    self.assertEqual(d, 0, 'Channel value should be exactly 0')
                ind = ind+1 
       	
        quasiClearWaveform(pv_trg, pv_smnm, pv_ai_stat)
        
        print 'Post trigger with positive trigger polarity'
        
        pv_d_trg.putw(0)
        time.sleep(0.5)
        
        # positive trigger polarity and no retriggering, post trigger mode
        pv_trgc.array_put("external0\0")
        pv_trgr.putw(0)
        pv_trgd.putw(0)
        
        pv_ai_msgs.array_put("START\0")
        
        #check that all but the first element of the waveforms are 0
        # checkEmptyWaveform(self, channel)
                
        #trigger
        time.sleep(1)
        pv_d_trg.putw(1)
                
        time.sleep(0.1)
        while pv_ai_stat.getw() == 4: # processing
            time.sleep(0.1)

        for ch, ref_v in zip(channel, val_local_single) :
            ch.array_get()
            ch.pend_io()
            v = ch.getValue()
            ind = 0
            for d in v:
                if ind < samples_count:
                    self.assertAlmostEqual(d, ref_v, places,
                                           ('Channel value read incorrect at '+
                                            '[{0}]{1} vs. {2}'.format(ind,d, ref_v)))
                    #print 'index: ' + str(ind) + ', d: ' + str(d) 
                else:
                    self.assertEqual(d, 0, 'Channel value should be exactly 0')
                ind = ind+1 
            
        quasiClearWaveform(pv_trg, pv_smnm, pv_ai_stat)
        
        print 'Post trigger with negative trigger polarity'
        
        # negative trigger polarity and no retriggering, post trigger mode
        pv_trgc.array_put("external0\\0\0")
        pv_trgr.putw(0)
        pv_trgd.putw(0)
        
        pv_ai_msgs.array_put("START\0")
        
        #check that all but the first element of the waveforms are 0
        # checkEmptyWaveform(self, channel)
                
        #trigger
        time.sleep(1)
        pv_d_trg.putw(0) 
                
        time.sleep(0.1)
        while pv_ai_stat.getw() == 4: # processing
            time.sleep(0.1)

        for ch, ref_v in zip(channel, val_local_single) :
            ch.array_get()
            ch.pend_io()
            v = ch.getValue()
            ind = 0
            for d in v:
                if ind < samples_count:
                    self.assertAlmostEqual(d, ref_v, places, 
                                           ('Channel value read incorrect at '+
                                            '[{0}]{1} vs. {2}'.format(ind,d, ref_v)))
                    #print 'index: ' + str(ind) + ', d: ' + str(d) 
                else:
                    self.assertEqual(d, 0, 'Channel value should be exactly 0')
                ind = ind+1        	
       	
       	quasiClearWaveform(pv_trg, pv_smnm, pv_ai_stat)
        
        # print 'Post trigger with retrigger (not implemented!!!)'
        # problems with manual tests. If re-triggering is enabled data acquisition is repeated 
        # after only one trigger.
        # This behavior is not wanted. Is re-triggering even needed with AutoRearm function?
        
        
        print 'Pre trigger' # sometimes zeros are found in the waveforms where normal data should be!!! 
          
        # pre trigger, positive trigger polarity
        pv_trgc.array_put("external0\0")
        pv_trgr.putw(0)
        pv_trgd.putw(-samples_count)
             
        pv_ai_msgs.array_put("START\0")
        
        #check that all but the first element of the waveforms are 0
        # checkEmptyWaveform(self, channel)
                
        #trigger
        time.sleep(1)
        pv_d_trg.putw(1)
        time.sleep(1)
        pv_d_trg.putw(0)                   
        
        time.sleep(0.1)
        while pv_ai_stat.getw() == 4: # processing
            time.sleep(0.1)

        for ch, ref_v in zip(channel, val_local_single) :
            ch.array_get()
            ch.pend_io()
            v = ch.getValue()
            ind = 0
            for d in v:
                if ind < samples_count:
                    self.assertAlmostEqual(d, ref_v, places,
                                           ('Channel value read incorrect at '+
                                            '[{0}]{1} vs. {2}'.format(ind,d, ref_v)))
                    #print 'index: ' + str(ind) + ', d: ' + str(d) 
                else:
                    self.assertEqual(d, 0, 'Channel value should be exactly 0')
                ind = ind+1        	
       	
       	quasiClearWaveform(pv_trg, pv_smnm, pv_ai_stat)
       	
       	print 'Middle trigger' # sometimes zeros are found in the waveforms where normal data should be!!!    
              
        # pre trigger, positive trigger polarity
        pv_trgc.array_put("external0\0")
        pv_trgr.putw(0)
        pv_trgd.putw(-samples_count/2)
        
        pv_ai_msgs.array_put("START\0")
        
        #check that all but the first element of the waveforms are 0
        # checkEmptyWaveform(self, channel)
                
        #trigger
        time.sleep(1)
        pv_d_trg.putw(1)
        time.sleep(1)
        pv_d_trg.putw(0)                      
        
        time.sleep(0.1)
        while pv_ai_stat.getw() == 4: # processing
            time.sleep(0.1)

        for ch, ref_v in zip(channel, val_local_single) :
            ch.array_get()
            ch.pend_io()
            v = ch.getValue()
            ind = 0
            for d in v:
            	#print 'ind: ' + str(ind) + 'val: ' + str(d)
                if ind < samples_count:
                    self.assertAlmostEqual(d, ref_v, places, 
                                           ('Channel value read incorrect at '+
                                            '[{0}]{1} vs. {2}'.format(ind,d, ref_v)))
                    #print 'index: ' + str(ind) + ', d: ' + str(d) 
                else:
                    self.assertEqual(d, 0, 'Channel value should be exactly 0')
                ind = ind+1        	
       	
       	quasiClearWaveform(pv_trg, pv_smnm, pv_ai_stat)           
        
        
        
              
           
#########################################################################################################        
#########################################################################################################
#########################################################################################################
        
    # Checking GCTR
    def test_TC_04(self):
        print 'TC-04 Testing General purpose timer/counter'
        print 'Connect DO0 to GP_TC_GATE'
        print 'Connect DO1 to GP_TC_CLK'
        print 'Connect DO2 to GP_TC_UPDN'
        print 'Connect DI0 to GP_TC_OUT'
        temp = raw_input('Press enter\n')
        
        pv_source = CaChannel() 
        pv_mode = CaChannel() 
        pv_gate = CaChannel() 
        pv_updn_source = CaChannel() 
        pv_updn = CaChannel() 
        pv_ctr = CaChannel() 
        pv_source.searchw(self.device+'-'+'GCTR-CLKS')
        pv_mode.searchw(self.device+'-'+'GCTR-PM')
        pv_gate.searchw(self.device+'-'+'GCTR-GTS')
        pv_updn_source.searchw(self.device+'-'+'GCTR-UDS')
        pv_updn.searchw(self.device+'-'+'GCTR-UDC')
        pv_ctr.searchw(self.device+'-'+'GCTR-COUNT')
        
        pv_read = CaChannel() 
        pv_read_proc = CaChannel() 
        pv_read.searchw(self.device+'-'+'GCTR-READ')
        pv_read_proc.searchw(self.device+'-'+'GCTR-READ.PROC')
        
        pv_msgs = CaChannel()
        pv_msgs.searchw(self.device+'-'+'GCTR-MSGS')
        
        
        pv_d_gate = CaChannel() 
        pv_d_clk = CaChannel() 
        pv_d_updn = CaChannel() 
        pv_d_out = CaChannel() 
        pv_d_out_proc = CaChannel() 
        pv_d_gate.searchw(self.device+'-'+'DO0-OUT')
        pv_d_clk.searchw(self.device+'-'+'DO1-OUT')
        pv_d_updn.searchw(self.device+'-'+'DO2-OUT')
        pv_d_out.searchw(self.device+'-'+'DI0-IN')
        pv_d_out_proc.searchw(self.device+'-'+'DI0-IN.PROC')
        
        #Counter processing mode
        pv_source.putw(1) # external clock source
        pv_mode.putw(0) # general counter mode
        pv_gate.putw(1) # external gate source
        pv_updn_source.putw(1) # external up/down control
        
        
        # Checking if counter resets after returning to on state
        print('Verify counter reset function')
        val_init = 100
        pv_ctr.putw(val_init) #initial counter value
        pv_msgs.array_put("STOP\0")
        time.sleep(0.1)
        pv_msgs.array_put("START\0")
        
        pv_read_proc.putw(1)
        time.sleep(0.1)
        self.assertTrue(pv_read.getw() == val_init, 'Initial counter value set incorrectly')
        
        pv_msgs.array_put("STOP\0")
        
        pv_read_proc.putw(1)
        time.sleep(0.1)
        self.assertTrue(pv_read.getw() == 0, 'Counter value was not reset after stopping the GCTR')
        
        
        #Counting down from val_init to 0, checking value and out
        print('Counting DOWN from 100 to 0')
        val_init = 100
        pv_d_clk.putw(0)
        pv_ctr.putw(val_init) #initial counter value
        pv_d_updn.putw(0) # counting down
        pv_d_gate.putw(1) # gate enables counting
        time.sleep(0.1)
        pv_msgs.array_put("START\0")
        
        time.sleep(0.1)
        
        for i in range(val_init, 0, -1):
            pv_read_proc.putw(1)
            pv_d_out_proc.putw(1)
            time.sleep(0.001)
            self.assertTrue(pv_d_out.getw() == 0, 'Out should be equal to 0')
            self.assertTrue(pv_read.getw() == i, 'Counter value incorrect')
            pv_d_clk.putw(1)
            time.sleep(0.001)
            pv_d_clk.putw(0)
        
        pv_read_proc.putw(1)
        pv_d_out_proc.putw(1)
        time.sleep(0.001)
        self.assertTrue(pv_d_out.getw() == 1, 'Out should be equal to 1')
        self.assertTrue(pv_read.getw() == 0, 'Counter value incorrect')
           
        pv_msgs.array_put("STOP\0")
        
        #Counting up from val_init to 2^16-1, checking value and out
        print('Counting UP from (2^16)-100 to (2^16)-1')
        val_end = 65535
        val_init = val_end - 100

        pv_d_clk.putw(0)
        pv_ctr.putw(val_init) #initial counter value
        pv_d_updn.putw(1) # counting up
        pv_d_gate.putw(1) # gate enables counting
        time.sleep(0.1)
        pv_msgs.array_put("START\0")
        
        time.sleep(0.1)
        
        for i in range(val_init, val_end+1):
            pv_read_proc.putw(1)
            pv_d_out_proc.putw(1)
            time.sleep(0.001)
            self.assertTrue(pv_d_out.getw() == 0, 'Out should be equal to 0')
            self.assertTrue(pv_read.getw() == i, 'Counter value incorrect')
            pv_d_clk.putw(1)
            time.sleep(0.001)
            pv_d_clk.putw(0)
        
        pv_read_proc.putw(1)
        pv_d_out_proc.putw(1)
        time.sleep(0.001)
        self.assertTrue(pv_d_out.getw() == 1, 'Out should be equal to 1')
        self.assertTrue(pv_read.getw() == 0, 'Counter value incorrect')
           
        pv_msgs.array_put("STOP\0")
        
        # Checking counting down with enabling/disabling gate
        print('Counting DOWN with enabling/disabling gate')
        val_init = 500
        pv_d_clk.putw(0)
        pv_ctr.putw(val_init) #initial counter value
        pv_d_updn.putw(0) # counting down
        pv_d_gate.putw(1) # gate enables counting
        time.sleep(0.1)
        pv_msgs.array_put("START\0")
        
        time.sleep(0.1)
        
        for i in range(val_init, 0,-1):
            
            time.sleep(0.001)

            pv_d_clk.putw(1)
            time.sleep(0.001)
            pv_d_clk.putw(0)
            if i%2:
                pv_d_gate.putw(1)
            else:
                pv_d_gate.putw(0)
                
        pv_read_proc.putw(1)
        time.sleep(0.001)        
        self.assertTrue(pv_read.getw() == val_init/2, 'Counter value incorrect')     
           
        pv_msgs.array_put("STOP\0")    
        
        
        #Rate generation mode
        print('Rate generation mode with external clock source')
        pv_source.putw(1) # external clock source
        pv_mode.putw(1) # general counter mode
        pv_gate.putw(1) # external gate source
        pv_updn_source.putw(1) # external up/down control
        
        pv_d_clk.putw(0)
        
        pv_d_updn.putw(0) # counting down
        pv_d_gate.putw(1) # gate enables counting
        
        val_init = 10
        pv_ctr.putw(val_init) #initial counter value
        
        time.sleep(0.1)
        pv_msgs.array_put("START\0")
        
        time.sleep(0.1)
           
        for j in range (0,10):
            for i in range(val_init, 0,-1):
                pv_d_clk.putw(1)
                time.sleep(0.001)
                pv_d_clk.putw(0)
                time.sleep(0.001)
                pv_read_proc.putw(1)
                time.sleep(0.001)
                pv_d_out_proc.putw(1)
                time.sleep(0.001)
                if pv_read.getw() == 1:
                	self.assertTrue(pv_d_out.getw() == 1, 'Out should be equal to 1')
                else:
                    self.assertTrue(pv_d_out.getw() == 0, 'Out should be equal to 0')     
        
        
        pv_msgs.array_put("STOP\0")           
        
        
#########################################################################################################        
#########################################################################################################
#########################################################################################################        

        
def quasiClearWaveform(pv_trg, pv_smnm, pv_ai_stat):
    tmp_smnm = pv_smnm.getw()
    pv_smnm.putw(1) # to make sure waveforms are almost empty
    pv_trg.putw(0)  # soft trigger
    time.sleep(0.1)
    while pv_ai_stat.getw() == 4: # processing
        time.sleep(0.1)
    pv_smnm.putw(tmp_smnm)
        
def checkEmptyWaveform(test, channel):
    for ch in channel :
         ch.array_get()
         ch.pend_io()
         v = ch.getValue()
         ind = 0
         for d in v:
             if ind > 0:
                 test.assertEqual(d, 0, 'Channel value should be exactly 0')
             ind = ind+1 	         
        
        
        
if __name__ == '__main__':
    unittest.main() 
